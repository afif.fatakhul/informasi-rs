<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\RuanganController;
use App\Http\Controllers\DokterController;
use App\Http\Controllers\AduanController;
use App\Http\Controllers\BannerAdsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'cekuser:admin'])->group(function () {
    Route::get('/', [AdminController::class, 'index']);

    Route::get('admin', [AdminController::class, 'index'])->name('home');;
    Route::resource('ruangan', RuanganController::class);
    Route::resource('dokter', DokterController::class);
    Route::post('del_dokter/{id}', [DokterController::class, 'destroy']);
    Route::get('jadwal_dokter/{dokterId}', [DokterController::class, 'editJadwal']);
    Route::post('input_jadwal/{dokterId}', [DokterController::class, 'inputJadwal']);
    
    Route::post('upload_image_ruangan', [RuanganController::class, 'uploadImage']);
    Route::get('fetch_image_ruangan/{ruanganId}', [RuanganController::class, 'fetchImage']);
    Route::get('delete_image_ruangan/{id}', [RuanganController::class, 'deleteImage']);

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('users', App\Http\Controllers\KelolaUserController::class);
    Route::resource('banners', BannerAdsController::class);
    Route::resource('kelola_pelayanan',  App\Http\Controllers\KelolaPelayananController::class);
    Route::get('kelola-not-direct/{pelayananId}',  [App\Http\Controllers\KelolaPelayananController::class, 'showNotDirect']);
    Route::post('simpan_foto_det_pel',  [App\Http\Controllers\KelolaPelayananController::class, 'simpanFotoDetPel']);
    Route::get('delete-not-direct/{id}',  [App\Http\Controllers\KelolaPelayananController::class, 'deleteFotoDetPel']);
    Route::post('update-detail-pelayanan-not-direct',  [App\Http\Controllers\KelolaPelayananController::class, 'storeDetPelNotDirect']);
    Route::post('update_pelayanan_direct',  [App\Http\Controllers\KelolaPelayananController::class, 'simpanDirectPelayanan']);
    Route::post('update_det_pelayanan_direct',  [App\Http\Controllers\KelolaPelayananController::class, 'storeDetPelDirect']);
    Route::get('delete-direct-pelayanan/{pelayananId}',  [App\Http\Controllers\KelolaPelayananController::class, 'delDirectPelayanan']);
    Route::post('simpan_new_pelayanan',  [App\Http\Controllers\KelolaPelayananController::class, 'storeNewPelayanan']);
    Route::post('simpan_not_direct_pel/{id}',  [App\Http\Controllers\KelolaPelayananController::class, 'storeNewNotDirect']);
    
});

Route::middleware(['auth'])->group(function () {
    Route::get('pengaduan', [AduanController::class, 'index']);
    Route::get('pengaduan/{id}', [AduanController::class, 'showAduan']);
    Route::get('histori_pengaduan', [AduanController::class, 'historiAduan']);
    Route::get('fetch_data_duahari_aduan', [AduanController::class, 'fetchDataDuaHari']);
    Route::get('fetch_data_aduan', [AduanController::class, 'fetchData']);
    Route::get('get_unsoundbell', [AduanController::class, 'getUnSoundBell']);

    Route::post('mute_sound_bell', [AduanController::class, 'mutedSoundBell']);
    Route::post('action_aduan', [AduanController::class, 'actionAduan']);
    Route::get('get_aduan_by/{id}', [AduanController::class, 'getAduan']);
    Route::post('konfirmasi_aduan', [AduanController::class, 'sendKonfirmasi']);
});


Auth::routes(['register' => false]);