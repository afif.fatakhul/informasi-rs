<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PoliController;
use App\Http\Controllers\Api\DokterController;
use App\Http\Controllers\Api\InfoTTController;
use App\Http\Controllers\Api\AduanController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ArtikelController;
use App\Http\Controllers\Api\PelayananController;
use App\Http\Controllers\Api\BannerAdsController;
use App\Http\Controllers\Api\SisbroController;
use App\Http\Controllers\Api\HistoryChatController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\PopupController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('fetch_poli', [PoliController::class, 'fetchPoli']);
Route::get('fetch_dokter_poli/{smf}', [PoliController::class, 'listDokter']);
Route::get('fetch_galeri_poli/{id}', [PoliController::class, 'fetchGaleriPoli']);
Route::get('fetch_dokter', [DokterController::class, 'fetchDokter']);
Route::get('fetch_smf', [DokterController::class, 'fetchSMF']);
Route::get('fetch_info_tt', [InfoTTController::class, 'fetchData']);
Route::get('fetch_galeri_poli_v2/{id}', [PoliController::class, 'fetchGaleriPoliv2']);
Route::get('fetch_popup', [PopupController::class, 'fetchPopup']);


Route::post('login', [AuthController::class, 'authenticate']);
Route::post('register', [AuthController::class, 'register']);

Route::get('fetch_artikel', [ArtikelController::class, 'fechArtikel']);
Route::get('fetch_pelayanan', [PelayananController::class, 'fetchIndex']);
Route::get('fetch_det_pelayanan/{id}/{tipe}', [PelayananController::class, 'detailIndex']);
Route::get('fetch_det_penunjang/{id}', [PelayananController::class, 'detailIndex2']);
Route::get('fetch_jadwal_dokter/{id}', [DokterController::class, 'fetchJadwal']);

Route::get('fetch_tarif_ranap', [PelayananController::class, 'fetchTarifRanap']);
Route::get('fetch_banner_ads', [BannerAdsController::class, 'fetchBanner']);

Route::post('upload_file', [AduanController::class, 'uploadFile']);
Route::get('reverse_location/{lat}/{long}', [AduanController::class, 'getReverseLocation']);

Route::get('fetch_time_daftar', [SisbroController::class, 'fetchTime']);
Route::post('send_data_pasien', [SisbroController::class, 'sendPasienFromSimrs']);
Route::get('get_mobile_user', [SisbroController::class, 'getDataUser']);
Route::get('list_user_booking/{userId}', [SisbroController::class, 'detailUserRM']);

Route::get('get_pasien/{rm}', [SisbroController::class, 'getPatient']);
Route::get('get_cara_bayar', [SisbroController::class, 'caraBayar']);
Route::get('get_daftar_poli', [SisbroController::class, 'getPoli']);

Route::get('send-mail', function () {
    $details = [
        'title' => 'Verifikasi Akun',
        'body' => 'Silahkan lakukan verifikasi akun'
    ];
    \Mail::to('fatakhulafi11@gmail.com')->send(new \App\Mail\VerifyAkun($details));
   
    dd("Email is Sent.");
});

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('get_user', [AuthController::class, 'get_user']);
    Route::get('edit_user', [AuthController::class, 'detailUser']);
    Route::get('fetch_panic', [AduanController::class, 'getUserPengajuan']);
    Route::post('update_user', [AuthController::class, 'updateUser']);
    Route::post('aduan', [AduanController::class, 'store']);
    Route::post('logout', [AuthController::class, 'logout']);

    Route::get('fetch_pasien/{id}', [SisbroController::class, 'fetchPasien']);
    Route::post('verifikasi_code', [SisbroController::class, 'verifikasiKode']);
    Route::get('fetch_pasien_booking', [SisbroController::class, 'fetchPasienBooking']);
    Route::post('daftar_sisbro', [SisbroController::class, 'daftar']);

    Route::get('fetch_my_booking', [SisbroController::class, 'daftarBookingSaya']);
    Route::get('fetch_my_booking_v2', [SisbroController::class, 'daftarBookingSayaV2']);
    Route::post('batal_my_booking', [SisbroController::class, 'batal']);

    Route::get('history_chat_user', [HistoryChatController::class, 'fetchDataHistoryChat']);
    Route::post('send_histori_msg', [HistoryChatController::class, 'sendHistoriMessage']);
    Route::post('send_read_msg', [HistoryChatController::class, 'sendReadMessage']);

    Route::get('fetch_aduan_histori_admin', [AduanController::class, 'fetchAduanHistoriAdmin']);

    Route::post('konfirmasi_aduan', [AduanController::class, 'sendKonfirmasi']);

    Route::get('fetch_notification_data', [NotificationController::class, 'fetchData']);
    Route::post('konfirmasi_tindak_lanjut', [AduanController::class, 'konfirmasiTindakLanjut']);
    
});

Route::post('verifikasi_otp', [AuthController::class, 'verifikasiOTP']);
Route::post('kirim_ulang_otp', [AuthController::class, 'kirimUlangOTP']);
Route::get('monitoring_antrean', [SisbroController::class, 'monitoringAntrian']);
Route::get('send_notification_to_admin/{userid}/{senderId}', [HistoryChatController::class, 'sendNotification']);

Route::post('find_number_wa', [AuthController::class, 'findNumberWA']);
Route::post('reset_password', [AuthController::class, 'resetPassword']);

use Carbon\Carbon;
Route::get('test', function(){
    $day = Carbon::parse('2022-10-9')->format('l');
    echo $day;
});