@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Tambah Dokter</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('dokter') }}">Dokter</a></li>
            <li class="breadcrumb-item active">Tambah Dokter</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form class="mt-3" method="POST" action="{{ route('dokter.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Nama Dokter</label>
                            <div class="col-sm-10">
                                <input type="text" id="nama" name="nama" class="form-control" required />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Foto Dokter</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="file" id="formFile" id="gambar" name="gambar" required />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Jenis</label>
                            <div class="col-sm-10">
                                <select class="jenis-dokter" name="id_jns" style="width: 100%" required>
                                    <option value="">Pilih Salah Satu</option>
                                    @foreach($jenis as $row)
                                        <option value="{{ $row->smf }}">{{ $row->smf }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Universitas</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" id="universitas" name="universitas" required />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Biografi</label>
                            <textarea class="form-control biografi" id="biografi" name="biografi"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary"><span class="fa fa-save"></span>
                            Simpan</button>
                        <a href="{{ url()->previous() }}" class="btn btn-secondary"><i
                                class="fas fa-chevron-double-left"></i> Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
    $('.jenis-dokter').select2();
    var select2 = $(".jenis-dokter").select2();
    select2.data('select2').$selection.css('height', '34px');
});
</script>
@endpush