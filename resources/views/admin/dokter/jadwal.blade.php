@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Tambah Dokter</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('dokter') }}">Dokter</a></li>
            <li class="breadcrumb-item active">Kelola Jadwal</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Kelola Jadwal Dokter</h4>
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get("success") }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <form action="{{ url('input_jadwal/'.$dokterId) }}" method="POST" class="row p-3">
                        @csrf
                        <input type="hidden" name="jadwalId" 
                            @if($jadwalSenin != null) value="{{ $jadwalSenin->id }}" @endif />
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hari</label>
                                <input type="text" class="form-control" name="hari" readonly value="Senin" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Awal</label>
                                <input type="time" name="jam_awal_prakter" class="form-control" 
                                    @if($jadwalSenin != null) value="{{ $jadwalSenin->jam_awal_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Akhir</label>
                                <input type="time" name="jam_akhir_prakter" class="form-control"
                                    @if($jadwalSenin != null) value="{{ $jadwalSenin->jam_akhir_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Aksi</label><br/>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </div>
                        </div>
                    </form>

                    <!-- Selasa -->
                    <form action="{{ url('input_jadwal/'.$dokterId) }}" method="POST" class="row p-3">
                        @csrf
                        <input type="hidden" name="jadwalId" 
                            @if($jadwalSelasa != null) value="{{ $jadwalSelasa->id }}" @endif />
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hari</label>
                                <input type="text" class="form-control" name="hari" readonly value="Selasa" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Awal</label>
                                <input type="time" name="jam_awal_prakter" class="form-control" 
                                    @if($jadwalSelasa != null) value="{{ $jadwalSelasa->jam_awal_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Akhir</label>
                                <input type="time" name="jam_akhir_prakter" class="form-control"
                                    @if($jadwalSelasa != null) value="{{ $jadwalSelasa->jam_akhir_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Aksi</label><br/>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </div>
                        </div>
                    </form>

                    <!-- Rabu -->
                    <form action="{{ url('input_jadwal/'.$dokterId) }}" method="POST" class="row p-3">
                        @csrf
                        <input type="hidden" name="jadwalId" 
                            @if($jadwalRabu != null) value="{{ $jadwalRabu->id }}" @endif />
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hari</label>
                                <input type="text" class="form-control" name="hari" readonly value="Rabu" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Awal</label>
                                <input type="time" name="jam_awal_prakter" class="form-control" 
                                    @if($jadwalRabu != null) value="{{ $jadwalRabu->jam_awal_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Akhir</label>
                                <input type="time" name="jam_akhir_prakter" class="form-control"
                                    @if($jadwalRabu != null) value="{{ $jadwalRabu->jam_akhir_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Aksi</label><br/>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </div>
                        </div>
                    </form>

                    <!-- Kamis -->
                    <form action="{{ url('input_jadwal/'.$dokterId) }}" method="POST" class="row p-3">
                        @csrf
                        <input type="hidden" name="jadwalId" 
                            @if($jadwalKamis != null) value="{{ $jadwalKamis->id }}" @endif />
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hari</label>
                                <input type="text" class="form-control" name="hari" readonly value="Kamis" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Awal</label>
                                <input type="time" name="jam_awal_prakter" class="form-control" 
                                    @if($jadwalKamis != null) value="{{ $jadwalKamis->jam_awal_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Akhir</label>
                                <input type="time" name="jam_akhir_prakter" class="form-control"
                                    @if($jadwalKamis != null) value="{{ $jadwalKamis->jam_akhir_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Aksi</label><br/>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </div>
                        </div>
                    </form>

                    <!-- Jumat -->
                    <form action="{{ url('input_jadwal/'.$dokterId) }}" method="POST" class="row p-3">
                        @csrf
                        <input type="hidden" name="jadwalId" 
                            @if($jadwalJumat != null) value="{{ $jadwalJumat->id }}" @endif />
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hari</label>
                                <input type="text" class="form-control" name="hari" readonly value="Jumat" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Awal</label>
                                <input type="time" name="jam_awal_prakter" class="form-control" 
                                    @if($jadwalJumat != null) value="{{ $jadwalJumat->jam_awal_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Akhir</label>
                                <input type="time" name="jam_akhir_prakter" class="form-control"
                                    @if($jadwalJumat != null) value="{{ $jadwalJumat->jam_akhir_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Aksi</label><br/>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </div>
                        </div>
                    </form>

                    <!-- Sabtu -->
                    <form action="{{ url('input_jadwal/'.$dokterId) }}" method="POST" class="row p-3">
                        @csrf
                        <input type="hidden" name="jadwalId" 
                            @if($jadwalSabtu != null) value="{{ $jadwalSabtu->id }}" @endif />
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Hari</label>
                                <input type="text" class="form-control" name="hari" readonly value="Sabtu" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Awal</label>
                                <input type="time" name="jam_awal_prakter" class="form-control" 
                                    @if($jadwalSabtu != null) value="{{ $jadwalSabtu->jam_awal_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jam Akhir</label>
                                <input type="time" name="jam_akhir_prakter" class="form-control"
                                    @if($jadwalSabtu != null) value="{{ $jadwalSabtu->jam_akhir_prakter }}" @endif required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Aksi</label><br/>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
