@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Dokter</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Dokter</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Ruangan</h5>
                    <a href="{{ route('dokter.create') }}" class="btn btn-primary">
                        Tambah Data
                    </a>
                    <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Nama
                                </th>
                                <th>
                                    SMF
                                </th>
                                <th>
                                    Univ
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($dokter as $row)
                            @php $no++; @endphp
                            <tr>
                                <td>
                                    {{ $no }}
                                </td>
                                <td>
                                    {{ $row->nama }}
                                </td>
                                <td>
                                    {{ $row->smf }}
                                </td>
                                <td>
                                    {{ $row->universitas }}
                                </td>
                                <td>
                                    <a href="{{ url('jadwal_dokter/'.$row->id) }}" class="btn btn-sm btn-info text-white mb-2">Jadwal Dokter</a>
                                    <a href="{{ route('dokter.edit', $row->id) }}" class="btn btn-sm btn-primary">
                                        <i class="ri-edit-box-line"></i> </a>
                                    <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal"
                                        data-bs-target="#basicModal{{ $row->id }}">
                                        <i class="ri-delete-bin-fill"></i>
                                    </button>
                                </td>
                            </tr>
                            <div class="modal fade" id="basicModal{{ $row->id }}" tabindex="-1" aria-hidden="true"
                                style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Konfirmasi Hapus Data</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Data yang dihapus akan hilang, pastikan anda memeriksa sebelum, melakukan
                                            hapus data ?
                                        </div>
                                        <div class="modal-footer">
                                            <form action="{{ url('del_dokter/'.$row->id) }}" class="mt-5" method="POST">
                                                @csrf
                                                <button type="submit"
                                                    class="btn btn-primary">Ya</button>
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Tidak</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
     $("#datatable").DataTable();
</script>
@endpush