@extends('templates.main')
@section('content')
<section class="section dashboard">
    <div class="row">

        <div class="col-md-4">
            <div class="card info-card sales-card">
                <div class="card-body">
                  <h5 class="card-title">Aduan Belum Diverifikasi</h5>
                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bi bi-bell-fill"></i>
                    </div>
                    <div class="ps-3">
                      <h6>{{ $waitingVerifikasi }}</h6>
                    </div>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card info-card revenue-card">
                <div class="card-body">
                  <h5 class="card-title">Aduan Valid</h5>
                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bx bxs-check-circle"></i>
                    </div>
                    <div class="ps-3">
                      <h6>{{ $valid }}</h6>
                    </div>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card info-card customers-card">
                <div class="card-body">
                  <h5 class="card-title">Aduan Tidak Valid</h5>
                  <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                        <i class="bx bxs-check-circle"></i>
                    </div>
                    <div class="ps-3">
                      <h6>{{ $notValid }}</h6>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
    
</script>
@endpush