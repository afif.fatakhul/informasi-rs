@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Data Users</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Users</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data User</h5>
                    <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Nama
                                </th>
                                <th>
                                    Username
                                </th>
                                <th>
                                    No HP
                                </th>
                                <th>
                                    Status User
                                </th>
                                <th>
                                    OTP Verifikasi
                                </th>
                                <th>
                                    Aksi
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($users as $row)
                            @php $no++; @endphp
                            <tr>
                                <td>
                                    {{ $no }}
                                </td>
                                <td>
                                    {{ $row->name }}
                                </td>
                                <td>
                                    {{ $row->email }}
                                </td>
                                <td>
                                    {{ $row->telepon }}
                                </td>
                                <td>
                                    @if($row->is_active == 1)
                                        Aktif
                                    @elseif($row->is_active == 0)
                                        Tidak Aktif 
                                    @else 
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($row->is_verified_otp == 0)
                                        Belum OTP
                                    @elseif($row->is_verified_otp == 1)
                                        Sudah OTP
                                    @else 
                                        -
                                    @endif
                                </td>
                                <td>
                                    <button id="btnDetail" data-id="{{ $row->id }}"
                                        class="btn btn-primary btn-sm">
                                        Detail</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="modalDetailUser" tabindex="-1" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail User</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <img id="loadImage" style="width: 100%;" />
            </div>
            <div class="col-md-6">
                <input id="user_id" type="hidden" />
                <table class="table">
                    <tr>
                        <td>NIK</td>
                        <td id="nik"></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td id="nama"></td>
                    </tr>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" onClick="verifikasi('tolak')" id="btnTolak">Tolak</button>
        <button type="button" class="btn btn-primary" onClick="verifikasi('terima')" id="btnTerima">Terima</button>
      </div>
    </div>
  </div>
</div>

@endsection

@push('scripts')
<script>
$("#datatable").DataTable();

$('body').on('click', '#btnDetail', function() {
    var data_id = $(this).data('id');
    $.ajax({
        url: '{{ url("users") }}' + '/' + data_id,
        type: 'GET',
        success: function(res) {
            console.log(res);
            $("#modalDetailUser").modal('show');
            $("#nik").html(res.data.nik);
            $("#nama").html(res.data.name);
            $("#user_id").val(res.data.id);
            $("#loadImage").attr("src", "{{ config('global.URL_ASSET_BARU') }}"+"ktp/"+res.data.foto_ktp);
        }
    });
});

function verifikasi(status) {
    var id_user = $("#user_id").val();
    $.ajax({
        url: '{{ route("users.store") }}',
        type: 'POST',
        data: {
            status: status,
            id: id_user,
        },
        success: function(res) {
            Swal.fire({
                title: 'Berhasil',
                text: "Data berhasil diverifikasi",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'OK'
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.reload();
                    }
            });
        }
    });
}

</script>
@endpush