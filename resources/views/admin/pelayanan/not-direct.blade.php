@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Daftar Ruangan</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Daftar Ruangan</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Daftar Ruangan</h5>
                    <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Tambah Data
                    </button>
                    <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Nama
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($detPelayanan as $row)
                            @php $no++; @endphp
                            <tr>
                                <td>
                                    {{ $no }}
                                </td>
                                <td>
                                    {{ $row->nama_det }}
                                </td>
                                <td>
                                    <a href="{{ url('kelola-not-direct/'.$row->id) }}" class="btn btn-sm btn-primary">
                                        <i class="ri-edit-box-line"></i> </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ url('simpan_not_direct_pel/'.Request::segment(2)) }}">
                    @csrf
                    <div class="form-group">
                        <label>Nama Pelayanan</label>
                        <input type="text" class="form-control" name="nama_det" />
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea class="form-control" name="deskripsi_pel"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
     $("#datatable").DataTable();
</script>
@endpush