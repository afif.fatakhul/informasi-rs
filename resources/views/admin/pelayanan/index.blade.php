@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Pelayanan</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Pelayanan</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Pelayanan</h5>
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <button type="button" class="btn btn-primary mb-3" data-bs-toggle="modal" data-bs-target="#addNewData">
                        Tambah Data
                    </button>
                    <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Nama
                                </th>
                                <th>
                                    Ditampilkan
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($pelayanan as $row)
                            @php $no++; @endphp
                            <tr>
                                <td>
                                    {{ $no }}
                                </td>
                                <td>
                                    {{ $row->nama }}
                                </td>
                                <td>
                                    {{ $row->is_active == 1 ? "Ditampilkan" : "Tidak Ditampilkan" }}
                                </td>
                                <td>
                                    <a href="{{ route('kelola_pelayanan.show', $row->id) }}" class="btn btn-sm btn-success">
                                        <i class="ri-camera-2-line"></i> </a>
                                    <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#editData{{ $row->id }}">
                                        <i class="ri-edit-box-line"></i> 
                                    </button>
                                </td>
                            </tr>

                            <div class="modal fade" id="editData{{ $row->id }}" tabindex="-1" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Pelayanan</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ url('simpan_new_pelayanan') }}">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $row->id }}" />
                                                <div class="form-group mb-3">
                                                    <label>Nama</label>
                                                    <input type="text" class="form-control" name="nama" value="{{ $row->nama }}" required />
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label>Tipe</label>
                                                    <select class="form-select" name="tipe" required>
                                                        <option value="">Pilih salah satu</option>
                                                        <option @if($row->tipe == "ranap") selected @endif value="ranap">Rawat Inap</option>
                                                        <option @if($row->tipe == "igd") selected @endif value="igd">IGD</option>
                                                        <option @if($row->tipe == "penunjang") selected @endif value="penunjang">Penunjang</option>
                                                        <option  @if($row->tipe == "rajal") selected @endif value="rajal">Rawat Jalan</option>
                                                    </select>
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label>Tipe Layout Android</label>
                                                    <select class="form-select" required id="tipeAndroid{{ $row->id }}" name="status" onchange="selectLayoutAndroidEdit('{{ $row->id }}')">
                                                        <option value="">Pilih salah satu</option>
                                                        <option @if($row->status == "direct") selected @endif value="direct">Langsung Tanpa Sub Menu</option>
                                                        <option @if($row->status == "not-direct") selected @endif value="not-direct">Dengan Sub Menu</option>
                                                        <option @if($row->status == "not-direct-tarif") selected @endif value="not-direct-tarif">Dengan Sub Menu Ada Item Tarif / Lainnya</option>
                                                        <option @if($row->status == "empty") selected @endif value="empty">Kosong (Seperti detail dokter di halaman home)</option>
                                                    </select>
                                                </div>
                                                <div class="form-group mb-3">
                                                    <label>Tampilkan</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="is_active"
                                                            @if($row->is_active == 1) selected @endif id="gridRadios1" value="1" checked>
                                                        <label class="form-check-label" for="gridRadios1">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="is_active"
                                                            @if($row->is_active == 0) selected @endif id="gridRadios2" value="0">
                                                        <label class="form-check-label" for="gridRadios2">Tidak</label>
                                                    </div>
                                                </div>
                                                <div class="form-group @if($row->status != 'direct') hidden @endif" id="rowDeskripsi{{ $row->id }}">
                                                    <label>Deskripsi</label>
                                                    <textarea class="form-control" name="deskripsi">{{ $row->deskripsi }}</textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            

                            @endforeach
                        </tbody>
                    </table>

                    <div class="modal fade" id="addNewData" tabindex="-1" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Pelayanan Baru</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                   <form method="POST" action="{{ url('simpan_new_pelayanan') }}">
                                        @csrf
                                        <input type="hidden" name="id" value="" />
                                        <div class="form-group mb-3">
                                            <label>Nama</label>
                                            <input type="text" class="form-control" name="nama" required />
                                        </div>
                                        <div class="form-group mb-3">
                                            <label>Tipe</label>
                                            <select class="form-select" name="tipe" required>
                                                <option value="">Pilih salah satu</option>
                                                <option value="ranap">Rawat Inap</option>
                                                <option value="igd">IGD</option>
                                                <option value="penunjang">Penunjang</option>
                                                <option value="rajal">Rawat Jalan</option>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label>Tipe Layout Android</label>
                                            <select class="form-select" required id="tipeAndroid" name="status" onchange="selectLayoutAndroid()">
                                                <option value="">Pilih salah satu</option>
                                                <option value="direct">Langsung Tanpa Sub Menu</option>
                                                <option value="not-direct">Dengan Sub Menu</option>
                                                <option value="not-direct-tarif">Dengan Sub Menu Ada Item Tarif / Lainnya</option>
                                                <option value="empty">Kosong (Seperti detail dokter di halaman home)</option>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label>Tampilkan</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="is_active" id="gridRadios1" value="1" checked>
                                                <label class="form-check-label" for="gridRadios1">Ya</label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="is_active" id="gridRadios2" value="0">
                                                <label class="form-check-label" for="gridRadios2">Tidak</label>
                                            </div>
                                        </div>
                                        <div class="form-group hidden" id="rowDeskripsi">
                                            <label>Deskripsi</label>
                                            <textarea class="form-control" name="deskripsi"></textarea>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
     $("#datatable").DataTable();

     function selectLayoutAndroid() {
        var tipeAndroid = $("#tipeAndroid").val();
        if(tipeAndroid == 'direct'){
            $("#rowDeskripsi").removeClass("hidden");
        }else{
            $("#rowDeskripsi").addClass("hidden");
        }
     }

     function selectLayoutAndroidEdit(id) {
        var tipeAndroid = $("#tipeAndroid"+id).val();
        if(tipeAndroid == 'direct'){
            $("#rowDeskripsi"+id).removeClass("hidden");
        }else{
            $("#rowDeskripsi"+id).addClass("hidden");
        }
     }
</script>
@endpush