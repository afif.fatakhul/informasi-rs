@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Pelayanan</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Pelayanan</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ Session::get('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Detail Pelayanan</h5>
                    <form method="POST" action="{{ url('update_pelayanan_direct') }}">
                        @csrf
                        <input type="hidden" name="id_pelayanan" value="{{ $detFotoPelayanan[0]->id_pelayanan }}" />
                        <div class="col-md-6 mb-3">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="nama" value="{{ $detFotoPelayanan[0]->nama }}" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Deskripsi</label>
                            <textarea class="form-control" name="deskripsi">{{ $detFotoPelayanan[0]->deskripsi }}</textarea>
                        </div>
                        <button class="btn btn-primary mt-3" type="submit">Simpan</button>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Foto</h5>
                    <button type="button" class="btn btn-primary mb-2" data-bs-toggle="modal" data-bs-target="#addNewData">
                        Tambah Foto
                    </button>
                    <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Link Foto
                                </th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($detFotoPelayanan as $row)
                            @php $no++; @endphp
                            <tr>
                                <td>
                                    {{ $no }}
                                </td>
                                <td>
                                    <img src="{{ $row->foto }}" width="100" />
                                </td>
                                <td>
                                    <button data-bs-toggle="modal" data-bs-target="#editData{{ $row->id }}" class="btn btn-sm btn-primary">
                                        <i class="ri-edit-box-line"></i> </button>
                                    <a href="{{ url('delete-direct-pelayanan/'.$row->id) }}" class="btn btn-danger btn-sm"
                                        onclick="return confirm('Apakah yakin mau hapus data ini ?')">
                                        <i class="bi bi-trash"></i>
                                    </a>
                                </td>
                            </tr>

                            <div class="modal fade" id="editData{{ $row->id }}" tabindex="-1" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Data</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{ url('update_det_pelayanan_direct') }}">
                                                @csrf
                                                <input type="hidden" name="id_det_pel" value="{{ $row->id }}" />
                                                <input type="hidden" name="action" value="edit" />
                                                <div class="form-group mb-3">
                                                    <label>Link Foto</label>
                                                    <input class="form-control" name="foto" value="{{ $row->foto }}" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Preview Foto</label><br/>
                                                    <img src="{{ $row->foto }}" width="200" />
                                                </div>
                                        </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            @endforeach
                        </tbody>
                    </table>

                    <div class="modal fade" id="addNewData" tabindex="-1" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Tambah Data</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ url('update_det_pelayanan_direct') }}">
                                        @csrf
                                        <input type="hidden" name="action" value="tambah" />
                                        <input type="hidden" name="id_pelayanan" value="{{ Request::segment(2) }}" />
                                        <div class="form-group">
                                            <label>Link Foto</label>
                                            <input class="form-control" name="foto" />
                                        </div>
                                </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
     $("#datatable").DataTable();
</script>
@endpush