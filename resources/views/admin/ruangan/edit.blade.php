@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Kelola Gambar Ruangan</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('ruangan') }}">Ruangan</a></li>
            <li class="breadcrumb-item active">Kelola Gambar Ruangan</li>
        </ol>
    </nav>
</div>

<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Ruangan</h5>
                    <div class="row mb-3">
                        <label for="inputNumber" class="col-sm-2 col-form-label">Gambar Upload</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="file" id="formFile" onchange="imageUploaded()"
                                type="file">
                        </div>
                    </div>
                    <h5 class="card-title">Daftar Foto</h5>
                    <div class="row" id="loadImage"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
let base64String = "";
let oriBase64 = "";

loadImage();

function loadImage() {
    $('#loadImage').empty();
    $.ajax({
        url: '{{ url("fetch_image_ruangan/".$id) }}',
        type: 'GET',
        success: function(response) {
            response.data.forEach(e => {
                $('#loadImage').append(
                    '<div class="col-md-3"><img class="img-poli" src="{{ config("global.URL_ASSET_BARU") }}' +'poli/'+
                    e.gambar_ruangan + '" /><button type="button" onClick="deleteImage(' + e
                    .id +
                    ')" class="btn btn-sm btn-danger position-absolute" style="margin-left: -40px; margin-top: 8px;"><i class="ri-delete-bin-fill"></i></button></div>'
                );
                console.log(e.gambar_ruangan);
            });
            console.log(response);
        }
    });
}

function deleteImage(id) {
    $.ajax({
        url: '{{ url("delete_image_ruangan") }}' + '/' + id,
        type: 'GET',
        success: function(response) {
            loadImage();
        }
    });
}

function imageUploaded() {
    var file = document.querySelector(
        'input[type=file]')['files'][0];

    var reader = new FileReader();
    console.log(reader);

    reader.onload = function() {
        base64String = reader.result.replace("data:", "")
            .replace(/^.+,/, "");

        imageBase64Stringsep = base64String;
        oriBase64 = reader.result;

        // alert(imageBase64Stringsep);
        console.log(base64String);
        console.log(oriBase64);

        $.ajax({
            url: '{{ url("upload_image_ruangan") }}',
            type: 'POST',
            data: {
                oriImage: oriBase64,
                fotoRuangan: base64String,
                ruanganId: '{{ $id }}'
            },
            success: function(res) {
                console.log(res);
                loadImage();
            }
        })
    }
    reader.readAsDataURL(file);
}
</script>
@endpush