@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Ruangan</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Ruangan</li>
        </ol>
    </nav>
</div>
<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Data Ruangan</h5>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#basicModal">
                        Tambah Data
                    </button>
                    <table class="table" id="datatable">
                        <thead>
                            <tr>
                                <th>
                                    No
                                </th>
                                <th>
                                    Kode
                                </th>
                                <th>
                                    Ruangan
                                </th>
                                <th>
                                    Kategori
                                </th>
                                <th>
                                    Aksi
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $no = 0; @endphp
                            @foreach($ruangan as $row)
                            @php $no++; @endphp
                            <tr>
                                <td>
                                    {{ $no }}
                                </td>
                                <td>
                                    {{ $row->code_ruangan }}
                                </td>
                                <td>
                                    {{ $row->nama_ruangan }}
                                </td>
                                <td>
                                    {{ $row->kategori }}
                                </td>
                                <td>
                                    <a href="{{ route('ruangan.edit', $row->id) }}"
                                        class="btn btn-primary btn-sm">
                                        <i class="ri-camera-fill"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="p-6 w-full bg-white rounded border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
    <div class="modal fade" id="basicModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('ruangan.store') }}">
                        @csrf
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Kode Ruangan</label>
                            <div class="col-sm-10">
                                <input type="text" id="code_ruangan" name="code_ruangan" class="form-control"
                                    required />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Ruangan</label>
                            <div class="col-sm-10">
                                <input type="text" id="nama_ruangan" name="nama_ruangan" class="form-control"
                                    required />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Kategori</label>
                            <div class="col-sm-10">
                                <select class="form-select" id="kategori" name="kategori" required>
                                    <option name="POLI">Poli</option>
                                    <option name="RANAP">Rawat Inap</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea id="deskripsi" name="deskripsi" class="form-control" required></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Fasilitas</label>
                            <div class="col-sm-10">
                                <textarea id="fasilitas" name="fasilitas" class="form-control" required></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
$("#datatable").DataTable();

const btnTambah = document.querySelector("#btnTambah");
btnTambah.addEventListener("click", function() {
    $("#judulModal").text("Tambah Data");
});
</script>
@endpush