@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Pengaduan Hari Ini</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Pengaduan Dua Hari Ini Terakhir</li>
        </ol>
    </nav>
</div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Data Dua Hari Ini Terakhir</h5>
                <audio id="soundEmergency" src='{{ asset("public/sound/sound-alarm.mp3") }}'></audio>
                <div class="table-responsive">
                    <table class="table table-border" style="width:100%;" id="datatable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Pengadu</td>
                                <td>Tanggal Pengaduan</td>
                                <td>Lokasi</td>
                                <td>Link Google Map</td>
                                <td>Status</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="responseTimeModal" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Response Time</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h6>Silahkan pilih tindakan untuk aduan pada tanggal <span id="tindakanTgl"></span></h6>
                    <h6 id="tindakanNama"></h6>
                    <input type="hidden" id="aduanId" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info hidden" onclick="actionKonfirm('otw')" id="btnBerangkat">Keberangkatan</button>
                    <button type="button" class="btn btn-warning hidden" onclick="actionKonfirm('arrived')" id="btnTibaLokasi">Tiba Lokasi</button>
                    <button type="button" class="btn btn-primary hidden" onclick="actionKonfirm('finish')" id="btnSelesai">Selesai</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script>
function loadSound() {
    $.ajax({
        url: '{{ url("get_unsoundbell") }}',
        type: 'GET',
        success: function(res) {
            const soundEmergency = document.getElementById("soundEmergency");
            if (parseInt(res.data) > 0) {
                Swal.fire({
                    title: '<strong>Ada Panggilan Masuk</strong>',
                    icon: 'info',
                    html: 'Silahkan cek panggilan yang baru masuk!',
                    focusConfirm: false,
                    confirmButtonText: 'OK',
                }).then((result) => {
                    if (result.isConfirmed) {
                        muteSoundBell();
                    }
                });
                soundEmergency.play();
            } else {
                soundEmergency.pause();
                soundEmergency.currentTime = 0;
            }
        }
    });
}

setInterval(function() {
    loadSound();
}, 1000);

function muteSoundBell() {
    $.ajax({
        url: '{{ url("mute_sound_bell") }}',
        type: 'POST',
        success: function(res) {
            console.log(res);
        }
    })
}

function sendActionAduan(idAduan) {
    Swal.fire({
        title: 'Verifikasi ?',
        text: "Verifikasi Kebenaran data apakah valid atau tidak ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        showDenyButton: true,
        showCancelButton: true,
        denyButtonColor: '#d33',
        cancelButtonColor: '#606060',
        confirmButtonText: 'Valid',
        denyButtonText: 'Tidak Valid'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '{{ url("action_aduan") }}',
                    type: 'POST',
                    data: {
                        id: idAduan,
                        status: "1"
                    },
                    success: function(res) {
                        Swal.fire(
                            'Berhasil!',
                            'Pengaduan berhasil diterima',
                            'success'
                        );
                        tableTransaction.draw();
                    }
                });
            }else if(result.isDenied){
                $.ajax({
                    url: '{{ url("action_aduan") }}',
                    type: 'POST',
                    data: {
                        id: idAduan,
                        status: "2"
                    },
                    success: function(res) {
                        Swal.fire(
                            'Berhasil!',
                            'Pengaduan berhasil ditolak',
                            'success'
                        );
                        tableTransaction.draw();
                    }
                });
            }
    });
}

var tableTransaction = $("#datatable").DataTable({
    ajax: '{{ url("fetch_data_duahari_aduan") }}',
    responsive: true,
    processing: true,
    serverSide: true,
    paging: false,
    ordering: false,
    info: false,
    oLanguage: {
        sZeroRecords: 'Data tidak tersedia'
    },
    columns: [{
            data: "id",
            render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        {
            data: "name"
        },
        {
            data: "created_at"
        },
        {
            data: "lokasi"
        },
        {
            data: "btnLokasi"
        },
        {
            data: "status"
        },
        {
            data: "btnAction"
        },
    ]
});

$('#datatable tbody').on( 'click', 'button#btnVerifikasi', function () {
    var data = tableTransaction.row( $(this).parents('tr') ).data();
    sendActionAduan(data.id);
});

$('#datatable tbody').on( 'click', 'button#btnKonfirmasi', function () {
    $("#btnBerangkat").addClass("hidden");
    $("#btnTibaLokasi").addClass("hidden");
    $("#btnSelesai").addClass("hidden");
    var dataTable = tableTransaction.row( $(this).parents('tr') ).data();
    $("#aduanId").val(dataTable.id);
    $.ajax({
        url: '{{ url("get_aduan_by") }}'+'/'+dataTable.id,
        method: 'GET',
        success: function(response){
            const {data} = response;
            if(data.departure_date == null){
                $("#btnBerangkat").removeClass("hidden");
            }
            // if(data.departure_date != null && data.arrive_to_destination == null){
            //     $("#btnTibaLokasi").removeClass("hidden");
            // }
            // if(data.departure_date != null && data.arrive_to_destination != null && data.finish_services == null){
            //     $("#btnSelesai").removeClass("hidden");
            // }
            if(data.departure_date != null  && data.finish_services == null){
                $("#btnSelesai").removeClass("hidden");
            }
            $("#responseTimeModal").modal('show');
            $("#tindakanNama").html(dataTable.name);
            $("#tindakanTgl").html(dataTable.created_at);
        }
    });
});

function actionKonfirm(status) {
    var aduanId = $("#aduanId").val();
    $.ajax({
        url: '{{ url("konfirmasi_aduan") }}',
        method: 'POST',
        data: {
            id: aduanId,
            status: status
        },
        success: function(response){
            $("#responseTimeModal").modal('hide');
            tableTransaction.draw();
            Swal.fire(
                'Berhasil!',
                'Melakukan konfirmasi aduan',
                'success'
            );
        }
    });
}

setInterval(function() {
    tableTransaction.draw();
}, 5000);
</script>
@endpush