@extends('templates.main')
@section('content')
<div class="pagetitle">
    <h1>Pengaduan</h1>
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Pengaduan</li>
        </ol>
    </nav>
</div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Detail Pengaduan</h5>
                    <div class="row">
                        <div class="col-md-6 mb-2">
                            <div class="form-group">
                                <label>Pengadu</label>
                                <input class="form-control" value="{{ $aduan->name }}" readonly />
                            </div>
                        </div>
                        <div class="col-md-6 mb-2">
                            <div class="form-group">
                                <label>Tanggal Pengaduan</label>
                                <input class="form-control" value="{{ $aduan->created_at }}" readonly />
                            </div>
                        </div>
                        <div class="col-md-6 mb-2">
                            <div class="form-group">
                                <label>Lokasi</label>
                                <input class="form-control" value="{{ $aduan->lokasi }}" readonly />
                            </div>
                        </div>
                        <div class="col-md-6 mb-2">
                            <div class="form-group">
                                <label>Link Google Map</label>
                                <?php 
                                    $linkLokasi = 'https://www.google.com/maps/place/'.$aduan->latitude.','.$aduan->longitude.'/@'.$aduan->latitude.','.$aduan->longitude.',13z';
                                    $buttonLokasi = '<a target="_blank" href="'.$linkLokasi.'" class="btn btn-primary btn-sm">Lihat Lokasi</a>';
                                ?>
                                <div>
                                    <?= $buttonLokasi ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-2">
                            <div class="form-group">
                                <label>Status</label>
                                <?php 
                                    if($aduan->status == 0){
                                        $status = '<span class="badge bg-secondary"><i class="bi bi-exclamation-octagon me-1"></i> Belum diverifikasi</span>';
                                    }
                                    if($aduan->status == 1){
                                        $status = '<span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> Valid</span>';
                                    }
                                    if($aduan->status == 2){
                                        $status = '<span class="badge bg-danger"><i class="bi bi-exclamation-triangle me-1"></i> Tidak Valid</span>';
                                    }
                                ?>
                                <div>
                                    <?= $status ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-2">
                            <div class="form-group">
                                <label>Response Time</label>
                                <ul>
                                    <li>Terkonfirmasi : {{ $aduan->response_time }}</li>
                                    <li>Keberangkatan : {{ $aduan->departure_date }}</li>
                                    <li>Terlayani : {{ $aduan->finish_services }}</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 mb-2">
                            <div class="form-group">
                                <label>File</label>
                                <div>
                                    @if($aduan->tipe_file == 'png')
                                        <img src="{{ config('global.URL_ASSET_BARU').'fileaduans/'.$aduan->link_file }}" class="img-aduan" />
                                    @else 
                                    <video style="width: 100%" height="500" controls>
                                        <source src="{{ config('global.URL_ASSET_BARU').'fileaduans/'.$aduan->link_file }}" type="video/mp4" />
                                    </video>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection