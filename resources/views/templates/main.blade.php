<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kelola Informasi RS</title>
    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet" />

    <!-- Vendor CSS Files -->
    <link href="{{ asset('public/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendor/quill/quill.snow.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendor/quill/quill.bubble.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/dataTables/DataTables-1.11.5/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('public/select2/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/sweatalert/sweetalert2.min.css') }}" />

    <!-- Template Main CSS File -->
    <link href="{{ asset('public/assets/css/style.css') }}" rel="stylesheet" />
    <style>
        .img-poli {
            width: 100%;
            height: 100%;
            object-fit: contain;
        }
        .img-aduan {
            width: 400px;
        }
        .img-dokter {
            width: 200px;
        }
        .hidden {
            display: none;
        }
    </style>

</head>

<body>
    @include('templates.navbar')
    @include('templates.sidebar')
    <main id="main" class="main">
        @yield('content')
    </main>
    @include('templates.footer')
    <!-- Vendor JS Files -->
    <script src="{{ asset('public/js/jquery-min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/chart.js/chart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('public/dataTables/DataTables-1.11.5/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/dataTables/DataTables-1.11.5/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendor/php-email-form/validate.js') }}"></script>
    <script src="{{ asset('public/select2/select2.min.js') }}"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('public/sweatalert/sweetalert2.min.js') }}"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>
    <script>
    tinymce.init({
        selector: 'textarea'
    });
    </script>

    @stack('scripts')
    <script src="{{ asset('public/assets/js/main.js') }}"></script>
</body>

</html>