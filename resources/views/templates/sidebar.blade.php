<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">
    <ul class="sidebar-nav" id="sidebar-nav">
        @if(Auth::user()->user_type == 'admin')
        <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) == 'dashboard' ? '' : 'collapsed' }}" href="{{ url('/') }}">
                <i class="bi bi-grid"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) == 'ruangan' ? '' : 'collapsed' }}" href="{{ url('ruangan') }}">
                <i class="ri-building-4-line"></i>
                <span>Ruangan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) == 'dokter' ? '' : 'collapsed' }}" href="{{ url('dokter') }}">
                <i class="bi bi-journal-text"></i><span>Dokter</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) == 'kelola_pelayanan' ? '' : 'collapsed' }}" href="{{ url('kelola_pelayanan') }}">
                <i class="bi bi-journal-text"></i><span>Pelayanan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) == 'users' ? '' : 'collapsed' }}" href="{{ url('users') }}">
                <i class="bi bi-journal-text"></i><span>Kelola User</span>
            </a>
        </li>
        @endif
        <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) == 'pengaduan' ? '' : 'collapsed' }}" href="{{ url('pengaduan') }}">
                <i class="ri-volume-up-fill"></i><span>Pengaduan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ Request::segment(1) == 'histori_pengaduan' ? '' : 'collapsed' }}" href="{{ url('histori_pengaduan') }}">
                <i class="ri-volume-up-fill"></i><span>Histori Pengaduan</span>
            </a>
        </li>
        
    </ul>
</aside><!-- End Sidebar-->