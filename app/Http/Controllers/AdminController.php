<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Aduan;

class AdminController extends Controller
{
    
    public function index()
    {
        $valid = $this->getDataAduan('1');
        $notValid = $this->getDataAduan('2');
        $waitingVerifikasi = $this->getDataAduan('0');
        return view('admin.index', compact('valid', 'notValid', 'waitingVerifikasi'));
    }

    public function getDataAduan($status)
    {
        $aduan = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')
            ->where('status', $status)
            ->count();
        return $aduan;
    }

}
