<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ruangan;
use App\Models\DetailRuangan;
use Carbon\Carbon;

class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ruangan = Ruangan::all();
        return view('admin.ruangan.index', compact('ruangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ruangan = Ruangan::create($request->all());
        return redirect()->back()->with('success', 'Berhasil menambahkan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.ruangan.edit', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadImage(Request $request)
    {
        $imageType = $this->getB64Type($request->oriImage);
        if($imageType == "image/png"){
            $tipe = ".png";
        }else if($imageType == "image/jpeg"){
            $tipe = ".jpeg";
        }else{
            $tipe = ".jpg";
        }

        $url = config('global.URL_SERVER_ASSET')."upload-foto-ruangan";

        $data = array(
            "fotoRuangan" => $request->fotoRuangan, 
            "tipe_file" => $tipe
        );
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        $result = json_decode($response, true);
        $fileName = $result['message'];
        DetailRuangan::create([
            "gambar_ruangan" => $fileName,
            "ruangan_id" => $request->ruanganId
        ]);
        return response()->json([
            "code" => 200,
            "message" => "Berhasil Menambahkan Foto"
        ]);
    }

    public function getB64Type($str) {
        // $str should start with 'data:' (= 5 characters long!)
        return substr($str, 5, strpos($str, ';')-5);
    }

    public function fetchImage($id)
    {
        $detailRuangan = DetailRuangan::where('ruangan_id', $id)->get();
        return response()->json([
            "code" => 200,
            "data" => $detailRuangan
        ]);
    }

    public function deleteImage($id)
    {
        $detailRuangan = DetailRuangan::find($id);
        $detailRuangan->delete();
        return response()->json(["code" => 200]);
    }

}
