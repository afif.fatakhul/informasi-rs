<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pelayanan;
use App\Models\Jadwal;
use DB;

class KelolaPelayananController extends Controller
{

    public function index()
    {
        $pelayanan = Pelayanan::all();
        return view('admin.pelayanan.index', compact('pelayanan'));
    }

    public function storeNewPelayanan(Request $request)
    {
        if($request->id == ""){
            $pelayanan = new Pelayanan();
            $message = "Berhasil menambah pelayanan baru";
        }else{
            $pelayanan = Pelayanan::find($request->id);
            $message = "Berhasil mengubah pelayanan baru";
        }
        $pelayanan->nama = $request->nama;
        $pelayanan->status = $request->status;
        $pelayanan->tipe = $request->tipe;
        if($request->status == 'not-direct-tarif' || $request->status == 'not-direct'){
            $pelayanan->deskripsi = "-";
        }else{
            $pelayanan->deskripsi = $request->deskripsi;
        }
        $pelayanan->is_active = $request->is_active;
        $pelayanan->save();
        return redirect()->back()->with('success', $message);
    }

    public function show($id)
    {
        $pelayanan = Pelayanan::find($id);
        $detPelayanan = DB::table('detail_pelayanan as dp')->where('dp.pelayanan_id', $id)
            ->get();
        if($pelayanan->status == 'not-direct' || $pelayanan->status == 'not-direct-tarif'){
            return view('admin.pelayanan.not-direct', compact('detPelayanan'));
        }else if($pelayanan->status == 'direct'){    
            $detFotoPelayanan = DB::table('pelayanan as pel')
                ->leftJoin('detail_pelayanan as dp', 'pel.id', '=', 'dp.pelayanan_id')
                ->select('dp.foto', 'dp.id', 'pel.id as id_pelayanan', 'pel.nama', 'pel.deskripsi', 'dp.nama_det')
                ->where('pel.id', $id)
                ->get();
            return view('admin.pelayanan.direct', compact('detFotoPelayanan'));
        }else if($pelayanan->status == 'empty'){
            return view('admin.pelayanan.empty', compact('pelayanan'));
        }
    }

    public function showNotDirect($pelayananId)
    {
        $detFotoPelayanan = DB::table('detail_pelayanan as dp')->leftJoin('foto_det_pel as foto', 'foto.id_det_pel', '=', 'dp.id')
            ->join('pelayanan as pel', 'pel.id', '=', 'dp.pelayanan_id')
            ->select('foto.foto_det_pel', 'foto.id', 'dp.id as id_pelayanan', 'dp.nama_det', 'dp.deskripsi_pel')
            ->where('dp.id', $pelayananId)
            ->get();
        return view('admin.pelayanan.kelola-not-direct', compact('detFotoPelayanan'));
    }

    public function storeNewNotDirect(Request $request, $id)
    {
        DB::table('detail_pelayanan')->insert([
            'nama_det' => $request->nama_det,
            'deskripsi_pel' => $request->deskripsi_pel,
            'pelayanan_id' => $id
        ]);
        return redirect()->back()->with('success', "Berhasil menambah pelayanan");
    }

    public function storeDetPelNotDirect(Request $request)
    {
        DB::table('detail_pelayanan')->where('id', $request->id_pelayanan)->update([
            'nama_det' => $request->nama_det,
            'deskripsi_pel' => $request->deskripsi_pel
        ]);
        return redirect()->back()->with('success', "Berhasil mengubah detail pelayanan");
    }

    public function simpanFotoDetPel(Request $request)
    {
        if($request->action == "tambah"){
            DB::table('foto_det_pel')->insert([
                'foto_det_pel' => $request->foto_det_pel,
                'id_det_pel' => $request->id_det_pel
            ]);
        }else if($request->action == "edit"){
            DB::table('foto_det_pel')->where('id', $request->id_det_pel)->update([
                'foto_det_pel' => $request->foto_det_pel
            ]);
        }
        return redirect()->back()->with('success', "Berhasil menambahkan data foto");
    }

    public function deleteFotoDetPel($id)
    {
        DB::table('foto_det_pel')->where('id', $id)->delete();
        return redirect()->back()->with('success', "Berhasil menghapus data foto");
    }

    public function simpanDirectPelayanan(Request $request)
    {
        $pelayanan = Pelayanan::find($request->id_pelayanan);
        $pelayanan->nama = $request->nama;
        $pelayanan->deskripsi = $request->deskripsi;
        $pelayanan->save();
        return redirect()->back()->with('success', "Berhasil menambahkan pelayanan");
    }

    public function storeDetPelDirect(Request $request)
    {
        if($request->action == "tambah"){
            DB::table('detail_pelayanan')->insert([
                'pelayanan_id' => $request->id_pelayanan,
                'foto' => $request->foto
            ]);
            return redirect()->back()->with('success', "Berhasil menambah detail pelayanan");
        }else if($request->action == "edit"){
            DB::table('detail_pelayanan')->where('id', $request->id_det_pel)->update([
                'foto' => $request->foto
            ]);
            return redirect()->back()->with('success', "Berhasil mengubah detail pelayanan");
        }
    }

    public function delDirectPelayanan($id)
    {
        DB::table('detail_pelayanan')->where('id', $id)->delete();
        return redirect()->back()->with('success', "Berhasil menghapus detail pelayanan");
    }

}
