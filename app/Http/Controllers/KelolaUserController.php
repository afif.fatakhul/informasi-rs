<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class KelolaUserController extends Controller
{
    
    public function index()
    {
        $users = User::where('user_type', 'mobile')
            ->orderBy('created_at', 'DESC')->get();
        return view('admin.users.index', compact('users'));
    }

    public function show($id)
    {
        $user = User::where('user_type', 'mobile')->where('id', $id)->first();
        return response()->json(["data" => $user]);
    }

    public function store(Request $request)
    {
        $active = "";
        if( $request->status == 'terima'){
            $active = 1;
        }else if($request->status == 'tolak'){
            $active = 0;
        }
        $user = User::find($request->id);
        $user->is_active = $active;
        $user->verifikator = \Auth::user()->id;
        $user->save();
        return response()->json(["code" => 200, "message" => "Berhasil verifikasi user"]);
    }

}
