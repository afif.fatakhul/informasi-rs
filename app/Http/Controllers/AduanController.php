<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Aduan;
use App\Models\User;
use App\Models\Notification;
use Carbon\Carbon;

class AduanController extends Controller
{
    
// Aduan adalah controller untuk Panik Button

    public function index()
    {
        return view('igd.index');
    }

    public function historiAduan()
    {
        return view('igd.histori');
    }

    public function getUnSoundBell()
    {
        $aduan = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')
            ->where('aduan.sound_bell', '0')
            ->select('count(*) as allcount')
            ->count();
        return response()->json(["code" => 200, "data" => $aduan]);
    }

    public function getAduan($id)
    {
        $aduan = Aduan::find($id);
        return response()->json(["code" => 200, "data" => $aduan]);
    }

    public function mutedSoundBell(Request $request)
    {
        $aduan = Aduan::query()->update([
            "sound_bell" => "1"
        ]);
        return response()->json(["code" => 200, "message" => "berhasil muted sound sistem"]);
    }

    public function actionAduan(Request $request)
    {
        $id = $request->id;
        $aduan = Aduan::find($id);
        $aduan->status = $request->status;
        $aduan->user_verifikator_id = \Auth::user()->id;
        $aduan->response_time = Carbon::now();
        $aduan->save();
        if($request->status == 1){
            $status = 'valid';
        }else if($request->status == 2){
            $status = 'not-valid';
        }
        $this->sendNotification("Pengaduan", $aduan->user_pengajuan_id, \Auth::user()->id, $status);
        return response()->json(["code" => 200, "message" => "berhasil melakukan action aduan"]);
    }

    public function showAduan($id)
    {
        $aduan = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')
            ->where('aduan.id', $id) 
            ->select('aduan.id','aduan.latitude', 'aduan.longitude', 'aduan.lokasi',
                'aduan.status', 'aduan.sound_bell', 'aduan.user_pengajuan_id', 'aduan.tipe_file', 'aduan.link_file',
                'aduan.user_verifikator_id', 'u.name', 'u.telepon', 'u.nik', 'aduan.created_at',
                'aduan.response_time', 'aduan.departure_date', 'aduan.arrive_to_destination', 'aduan.finish_services')
            ->first();
        return view('igd.show', compact('aduan'));
    }

    public function fetchDataDuaHari(Request $request)
    {
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $search_arr = $request->get('search');

        $searchValue = $search_arr['value'];

        $aduans = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')
            ->orderBy('aduan.status', 'ASC')
            ->where('u.name', 'like', '%' .$searchValue . '%')
            ->whereDate('aduan.created_at', Carbon::today())
            ->orWhereDate('aduan.created_at', Carbon::yesterday())
            ->select('aduan.id','aduan.latitude', 'aduan.longitude', 'aduan.lokasi',
                'aduan.status', 'aduan.sound_bell', 'aduan.user_pengajuan_id',
                'aduan.user_verifikator_id', 'u.name', 'u.telepon', 'u.nik', 'aduan.created_at', 'aduan.finish_services')
            ->get();
        $data = array();
        foreach ($aduans as $row) {
            $linkLokasi = 'https://www.google.com/maps/place/'.$row->latitude.','.$row->longitude.'/@'.$row->latitude.','.$row->longitude.',13z';
            $buttonLokasi = '<a target="_blank" href="'.$linkLokasi.'" class="btn btn-primary btn-sm">Lihat Lokasi</a>';
            $pengadu = 'Nama : '.$row->name.'<br/>'.'NIK : '.$row->nik.'<br/>Telepon : '.$row->telepon;

            if($row->status == 0){
                $status = '<span class="badge bg-secondary"><i class="bi bi-exclamation-octagon me-1"></i> Belum diverifikasi</span>';
                $buttonAction =  '<button id="btnVerifikasi" class="btn btn-sm btn-success">Verifikasi</button> 
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>';
            }
            if($row->status == 1 && $row->finish_services == null){
                $status = '<span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> Valid</span>';
                $buttonAction =  '<button id="btnSdhVerifikasi" class="btn btn-sm btn-info text-white">Terverifikasi</button>
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>
                    <button id="btnKonfirmasi" class="btn btn-sm btn-danger">Konfirmasi</button>';
            }
            if($row->status == 1 && $row->finish_services != null){
                $status = '<span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> Valid</span>
                <span class="badge bg-info"><i class="bi bi-check-circle me-1"></i> Selesai</span>';
                $buttonAction =  '<button id="btnSdhVerifikasi" class="btn btn-sm btn-info text-white">Terverifikasi</button>
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>';
            }
            if($row->status == 2){
                $status = '<span class="badge bg-danger"><i class="bi bi-exclamation-triangle me-1"></i> Tidak Valid</span>';
                $buttonAction =  '<button id="btnSdhVerifikasi" class="btn btn-sm btn-info text-white">Terverifikasi</button>
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>';
            }
            array_push($data, [
                "id" => $row->id,
                "name" => $pengadu,
                "created_at" => formatTanggalJamSistem($row->created_at),
                "lokasi" => $row->lokasi,
                "latitude" => $row->latitude,
                "longitude" => $row->longitude,
                "linkLokasi" => $linkLokasi,
                "btnLokasi" => $buttonLokasi,
                "status" => $status,
                "btnAction" => $buttonAction
            ]);
        }
        return response()->json([
            "code" => 200,  
            "data" => $data
        ]);
    }

    public function fetchData(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $totalRecords = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')
            ->orderBy('aduan.status', 'ASC')
            ->select('count(*) as allcount')
            ->count();
        $totalRecordswithFilter = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')
            ->orderBy('aduan.status', 'ASC')
            ->where('u.name', 'like', '%' .$searchValue . '%')
            ->count();
        $aduans = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')
            ->orderBy('aduan.status', 'ASC')
            ->skip($start)
            ->take($rowperpage)
            ->where('u.name', 'like', '%' .$searchValue . '%')
            ->select('aduan.id','aduan.latitude', 'aduan.longitude', 'aduan.lokasi',
                'aduan.status', 'aduan.sound_bell', 'aduan.user_pengajuan_id',
                'aduan.user_verifikator_id', 'u.name', 'u.telepon', 'u.nik', 'aduan.created_at',
                'aduan.response_time', 'aduan.departure_date', 'aduan.arrive_to_destination', 'aduan.finish_services')
            ->get();
        $data = array();
        foreach ($aduans as $row) {
            $linkLokasi = 'https://www.google.com/maps/place/'.$row->latitude.','.$row->longitude.'/@'.$row->latitude.','.$row->longitude.',13z';
            $buttonLokasi = '<a target="_blank" href="'.$linkLokasi.'" class="btn btn-primary btn-sm">Lihat Lokasi</a>';
            $pengadu = 'Nama : '.$row->name.'<br/>'.'NIK : '.$row->nik.'<br/>Telepon : '.$row->telepon;

            $status = "";
            $buttonAction = "";
            if($row->status == 0){
                $status .= '<span class="badge bg-secondary"><i class="bi bi-exclamation-octagon me-1"></i> Belum diverifikasi</span>';
                $buttonAction .=  '<button id="btnVerifikasi" class="btn btn-sm btn-success">Verifikasi</button> 
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>';
            }
            if($row->status == 1 && $row->finish_services == null){
                $status = '<span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> Valid</span>';
                $buttonAction =  '<button id="btnSdhVerifikasi" class="btn btn-sm btn-info text-white">Terverifikasi</button>
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>
                    <button id="btnKonfirmasi" class="btn btn-sm btn-danger">Konfirmasi</button>';
            }
            if($row->status == 1 && $row->finish_services != null){
                $status = '<span class="badge bg-success"><i class="bi bi-check-circle me-1"></i> Valid</span>
                <span class="badge bg-info"><i class="bi bi-check-circle me-1"></i> Selesai</span>';
                $buttonAction =  '<button id="btnSdhVerifikasi" class="btn btn-sm btn-info text-white">Terverifikasi</button>
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>';
            }
            if($row->status == 2){
                $status = '<span class="badge bg-danger"><i class="bi bi-exclamation-triangle me-1"></i> Tidak Valid</span>';
                $buttonAction =  '<button id="btnSdhVerifikasi" class="btn btn-sm btn-info text-white">Terverifikasi</button>
                    <a href="'.url('pengaduan/'.$row->id).'" target="_blank" class="btn btn-sm btn-primary">Detail</a>';
            }
            array_push($data, [
                "id" => $row->id,
                "name" => $pengadu,
                "created_at" => formatTanggalJamSistem($row->created_at),
                "lokasi" => $row->lokasi,
                "latitude" => $row->latitude,
                "longitude" => $row->longitude,
                "linkLokasi" => $linkLokasi,
                "btnLokasi" => $buttonLokasi,
                "status" => $status,
                "btnAction" => $buttonAction
            ]);
        }
        return response()->json([
            "code" => 200,  
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "data" => $data
        ]);
    }

    public function sendKonfirmasi(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $aduan = Aduan::find($id);
        if($status == 'otw'){
            $aduan->departure_date = Carbon::now();
        // }elseif($status == 'arrived'){
        //     $aduan->arrive_to_destination = Carbon::now();
        }elseif($status == 'finish'){
            $aduan->finish_services = Carbon::now();
        }
        $aduan->user_verifikator_id = \Auth::user()->id;
        $aduan->save();

        $this->sendNotification("Pengaduan", $aduan->user_pengajuan_id, \Auth::user()->id, $status);

        return response()->json(["code" => 200, "message" => "berhasil melakukan konfirmasi"]);
    }

    public function sendNotification($title, $userId, $sender, $status)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token = "AAAAKi29oJI:APA91bGG5psJXoSKNItXtEeunf8XQo6aGB74fiq4TLTa1G5_VuAiB-nkThcaNQ1ZbFS1Rfm6bDpJ2cNptbBbLfrwtVnbXoxO29Esm8hzxKC2p9_24tAeAfdvf0OVUUsgq11VVl1g0i-5";

        $user = User::find($userId);
        $userSender = User::find($sender);

        $message = "";
        if($status == "not-valid"){
            $message = $userSender->name." telah menolak pengaduan anda";
        }elseif($status == "valid") {
            $message = $userSender->name." telah menerima pengaduan anda";
        }elseif($status == "otw"){
            $message = "Pengaduan anda telah diterima \ndan sedang dalam proses perjalanan";
        }elseif($status == "arrived"){
            $message = "Pengaduan anda telah diterima \ndan telah sampai di tujuan";
        }elseif($status == "finish"){
            $message = "Pengaduan anda telah diterima \ndan telah berhasil diselesaikan";
        }

        Notification::create([
            "user_id" => $userId,
            "content" => $message,
            "title" => $title,
            "is_readed" => 0
        ]);

        $notification = [
            'tipeNotifikasi' => "aduan",
            'userId' => $userId,
            'title' => $title,
            'namaUser' => $user->name,
            'senderUser' => $userSender->name,
            'status' => $status,
            'message' => $message
        ];

        // $notificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, 
            'to' => "/topics/ChattingApp",
            'data' => $notification
        ];

        $headers = [
            'Content-Type: application/json',
            'Authorization: key='.$token,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $response = curl_exec($ch);
        curl_close($ch);
        return response()->json($response);
    }

}