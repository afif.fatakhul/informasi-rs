<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BannerAds;

class BannerAdsController extends Controller
{
    
    public function index()
    {
        $banners = BannerAds::all();
        return view('admin.banner.index', compact('banners'));
    }

    public function create()
    {
        return view('admin.banner.create');
    }

    public function edit($id)
    {
        $banner = BannerAds::find($id);
        return view('admin.banner.edit', compact('banner'));
    }

    public function store(Request $request)
    {
        $resorce = $request->file('image_ads');
        $fileName = time().$resorce->getClientOriginalName();
        $resorce->move(\base_path()."/public/bannerads", $fileName);

        $banner = new BannerAds();
        $banner->title_ads = $request->title_ads;
        $banner->image_ads = $fileName;
        $banner->deskripsi_ads = $request->deskripsi_ads;
        $banner->is_active = $request->is_active;
        $banner->save();

        return redirect('banners')->with("success", "Berhasil menambahkan banner");
    }

    public function update(Request $request, $id)
    {
        $banner = BannerAds::find($id);
        $resorce = $request->file('image_ads');
        if($resorce != ""){
            \File::delete(\base_path()."/public/bannerads/".$banner->image_ads);
            $fileName = time().$resorce->getClientOriginalName();
            $resorce->move(\base_path()."/public/bannerads", $fileName);
            $banner->image_ads = $fileName;
        }
        $banner->title_ads = $request->title_ads;
        $banner->deskripsi_ads = $request->deskripsi_ads;
        $banner->is_active = $request->is_active;
        $banner->save();
        return redirect('banners')->back()->with("success", "Berhasil mengubah banner");
    }

    public function destroy($id)
    {
        $banner = BannerAds::find($id);
        \File::delete(\base_path()."/public/bannerads/".$banner->image_ads);
        $banner->delete();
        return redirect('banners')->back()->with("success", "Berhasil menghapus banner");
    }

}
