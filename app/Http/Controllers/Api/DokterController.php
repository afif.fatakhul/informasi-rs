<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dokter;
use App\Models\JadwalDokter;

class DokterController extends Controller
{

    public function fetchSMF(Request $request){
        $search = $request->search;
        $dokter = Dokter::select('smf')
            ->where('smf', 'like', '%'.$search.'%')
            ->distinct()
            ->get();
        return response()->json(["code" => 200, "data" => $dokter]);
    }

    public function fetchDokter(Request $request)
    {
        $search = $request->search;
        $smf = $request->smf;
        $jumlahData = Dokter::where('nama', 'like', '%'.$search.'%')->count();
        $dokter = Dokter::select('id', 'nama', \DB::raw('CONCAT("'.config('global.URL_ASSET_BARU').'dokter/'.'", gambar) as image_url'), 
            'gambar', 'smf as jenis', 'biografi', 'universitas')
            ->where('nama', 'like', '%'.$search.'%')
            ->where('smf', $smf)
            ->get();
        return response()->json(["code" => 200, "data" => $dokter, "jumlahData" => $jumlahData]);
    }

    public function fetchJadwal($dokterId)
    {
        $jadwalDokter = JadwalDokter::where('dokter_id', $dokterId)->get();
        return response()->json(["code" => 200, "data" => $jadwalDokter]);
    }

}
