<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BannerAds;

class BannerAdsController extends Controller
{
    
    public function fetchBanner()
    {
        $data = [];
        $banners = BannerAds::where('is_active', 1)
            ->select(\DB::raw('CONCAT("'.config('global.BANNER_URL_IMG').'", image_ads) as image_url'), 'deskripsi_ads', 'id',
                'updated_at', 'title_ads')
            ->get();
        foreach ($banners as $row) {
            array_push($data, [
                'image_url' => $row->image_url,
                'deskripsi_ads' => $row->deskripsi_ads,
                'id' => $row->id,
                'updated_at' => tanggalForHuman($row->updated_at),
                'title_ads' => $row->title_ads
            ]);
        }
        return response()->json(["code" => 200, "data" => $data]);
    }

}
