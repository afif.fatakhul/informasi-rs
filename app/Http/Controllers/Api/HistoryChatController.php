<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HistoryChat;
use App\Models\Notification;
use App\Models\User;
use JWTAuth;

class HistoryChatController extends Controller
{

    public function sendHistoriMessage(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $cekHistori = HistoryChat::where('sender_id', $user->id)
                ->count();
            if($cekHistori == 0){
                HistoryChat::create([
                    'sender_id' => $user->id,
                    'receiver_id' => $request->receiver_id,
                    'is_readed' => 0
                ]);
            }
            $this->sendNotification("Pesan Baru", $request->receiver_id, $user->id);
            return response()->json(['code' => 200, 'message' => 'Berhasil tambah histori chat']);
        }catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function sendReadMessage(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $cekHistori = HistoryChat::where('sender_id', $request->user_id)
                ->update([
                    'is_readed' => 1
                ]);
            $this->sendNotification("Pesan Baru",$request->user_id, $user->id);
            return response()->json(['code' => 200, 'message' => 'Berhasil update baca pesan']);
        }catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    public function fetchDataHistoryChat(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $histories = HistoryChat::join('users as u', 'u.id', '=', 'history_chat.sender_id')
                ->where('history_chat.receiver_id', $user->id)
                ->select('history_chat.sender_id', 'history_chat.receiver_id', 'u.name', 
                    'history_chat.is_readed','history_chat.created_at')
                ->get();
            return response()->json(["code" => 200, "data" => $histories]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function sendNotification($title, $userId, $sender)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token = "AAAAKi29oJI:APA91bGG5psJXoSKNItXtEeunf8XQo6aGB74fiq4TLTa1G5_VuAiB-nkThcaNQ1ZbFS1Rfm6bDpJ2cNptbBbLfrwtVnbXoxO29Esm8hzxKC2p9_24tAeAfdvf0OVUUsgq11VVl1g0i-5";

        $user = User::find($userId);
        $userSender = User::find($sender);

        $message = $userSender->name." telah mengirim pesan kepada anda";

        $notification = [
            'tipeNotifikasi' => "chatting",
            'title' => $title,
            'userId' => $userId,
            'message' => $message,
            'namaUser' => $user->name,
            'senderUser' => $userSender->name
        ];

        // $notificationData = ["message" => $notification,"moredata" =>'dd'];


        Notification::create([
            "user_id" => $userId,
            "content" => $message,
            "title" => $title,
            "is_readed" => 0
        ]);

        $fcmNotification = [
            //'registration_ids' => $tokenList, 
            'to' => "/topics/ChattingApp",
            'data' => $notification
        ];

        $headers = [
            'Content-Type: application/json',
            'Authorization: key='.$token,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $response = curl_exec($ch);
        curl_close($ch);
        return response()->json($response);
    }


}
