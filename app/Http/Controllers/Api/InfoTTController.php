<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InfoTTController extends Controller
{
    
    public function fetchData(Request $request)
    {
        $search = $request->search;
        $curl_ruangan = curl_init();
        curl_setopt_array($curl_ruangan, array(
            CURLOPT_URL => "http://192.168.100.88/promedika/public/tt-list",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response_ruangan = curl_exec($curl_ruangan);
        curl_close($curl_ruangan);
        $ruangan = json_decode($response_ruangan, true);
        $data = [];
        foreach ($ruangan['data'] as $row) {
            if($search != ""){
                if(strpos($row["ruangan"], $search) != false){
                    array_push($data, [
                        "ruangan" => $row["ruangan"],
                        "TOTAL-TT" => $row["TOTAL-TT"],
                        "VVIP" => $row["VVIP"],
                        "NONCLASS" => $row["NONCLASS"],
                        "KELAS2" => $row["KELAS2"],
                        "VIP" => $row["VIP"],
                        "KELAS3" => $row["KELAS3"],
                        "KELAS1" => $row["KELAS1"],
                        "KONSERVATIF" => $row["KONSERVATIF"],
                        "ICU" => $row["ICU"],
                        "ISOLASI" => $row["ISOLASI"],
                        "ICCU" => $row["ICCU"],
                        "PARU" => $row["PARU"],
                        "HCU" => $row["HCU"],
                        "NICU" => $row["NICU"],
                        "ROI" => $row["ROI"]
                    ]);
               }
            }
            else {
                array_push($data, [
                    "ruangan" => $row["ruangan"],
                    "TOTAL-TT" => $row["TOTAL-TT"],
                    "VVIP" => $row["VVIP"],
                    "NONCLASS" => $row["NONCLASS"],
                    "KELAS2" => $row["KELAS2"],
                    "VIP" => $row["VIP"],
                    "KELAS3" => $row["KELAS3"],
                    "KELAS1" => $row["KELAS1"],
                    "KONSERVATIF" => $row["KONSERVATIF"],
                    "ICU" => $row["ICU"],
                    "ISOLASI" => $row["ISOLASI"],
                    "ICCU" => $row["ICCU"],
                    "PARU" => $row["PARU"],
                    "HCU" => $row["HCU"],
                    "NICU" => $row["NICU"],
                    "ROI" => $row["ROI"]
                ]);
            }
        }
        return response()->json(["code" => 200, "data" => $data]);
    }

}
