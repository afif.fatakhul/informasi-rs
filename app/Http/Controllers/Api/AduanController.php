<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aduan;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Notification;
use JWTAuth;

class AduanController extends Controller
{
    
// Aduan adalah untuk panik button

public function uploadFile(Request $request)
{
    $url = config('global.URL_SERVER_ASSET')."upload-video-aduan";

    $fileName = time().$_FILES['file']['name'];
    
    //if (isset($_FILES['file']['tmp_name'])){
        $fields = [
            'filedata' => new \CURLFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $fileName)
        ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        if(curl_exec($ch)){
            return response()->json(["code" => 200, "message" => $fileName, "success" => true]);  
        }else {
            echo "Error!" . curl_error($ch);
        }
    //}
}

public function store(Request $request)
{
    try {
        $user = JWTAuth::authenticate($request->token);

        if($user->is_active == 1){
            $folderPath = "fileaduans/";
            if($request->tipe_file == 'png'){
                $url = config('global.URL_SERVER_ASSET')."upload-foto-aduan";
                $data = array(
                    "link_file" => $request->link_file, 
                    "tipe_file" => $request->tipe_file,
                );
                
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec($curl);
                $result = json_decode($response, true);
                $fileName = $result['message'];
            }else{
                $fileName = $request->link_file;
            }
    
            $lokasi = $this->getReverseLocation($request->latitude, $request->longitude);
    
            $aduan = Aduan::create([
                'user_pengajuan_id' => $user->id,
                "latitude" => $request->latitude,
                "longitude" => $request->longitude,
                "lokasi" => $lokasi,
                "link_file" => $fileName,
                "tipe_file" => $request->tipe_file,
                "keterangan" => $request->keterangan,
                "status" => '0',
                "sound_bell" => '0'
            ]);
            $this->sendNotification("Tindak Lanjut", 12, $user->id, "aduan_baru");
            return response()->json(["code" => 200, "message" => "Berhasil menambahkan aduan", "success" => true]);
        }else{
            return response()->json(["code" => 200, "message" => "User anda tidak aktif", "success" => false]);
        }
    } catch (JWTException $exception) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, user cannot be logged out'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}

public function getReverseLocation($lat, $long)
{
    $curl_map_box = curl_init();
    curl_setopt_array($curl_map_box, array(
        CURLOPT_URL => config('global.MAP_BOX_URL').$long.','.$lat.'.json?access_token='.config('global.MAP_BOX_KEY'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
        ),
    ));
    $response_map_box = curl_exec($curl_map_box);
    curl_close($curl_map_box);
    $data = json_decode($response_map_box, true);
    return $data['features'][0]["place_name"];
}

public function getUserPengajuan(Request $request)
{
    try {
        $user = JWTAuth::authenticate($request->token);
        $aduan = Aduan::where('user_pengajuan_id', $user->id)
            ->select('id','latitude', 'longitude', 'lokasi', 
                'link_file', 'tipe_file', 'status', 'created_at', 'updated_at', 'lokasi',
                'response_time', 'departure_date', 'arrive_to_destination', 'finish_services')
            ->orderBy('created_at', 'DESC')
            ->get();
        $data = [];
        foreach ($aduan as $row) {
            if($row->status == '0'){
                $status = "Menunggu Verifikasi";
            }elseif ($row->status == '1') {
                $status = "Terverifikasi";
            }else{
                $status = "Tidak valid";
            }
            array_push($data, [
                'id' => $row->id,
                'latitude' => $row->latitude,
                'longitude' => $row->longitude,
                'lokasi' => $row->lokasi,
                'link_file' => $row->link_file,
                'url_asset_image' => config('global.URL_ASSET_BARU').'fileaduans/'.$row->link_file,
                'tipe_file' => $row->tipe_file,
                'alamat' => $row->lokasi,
                'status' => $status,
                'response_time' => $row->response_time != null ? tanggalForHuman($row->response_time) : "-",
                'departure_date' => $row->departure_date != null ? tanggalForHuman($row->departure_date) : "-",
                'arrive_to_destination' => $row->arrive_to_destination != null ? tanggalForHuman($row->arrive_to_destination) : "-",
                'finish_services' => $row->finish_services != null ? tanggalForHuman($row->finish_services) : "-",
                'created_at' => tanggalForHuman($row->created_at),
                'updated_at' => tanggalForHuman($row->updated_at)
            ]);
        }
        return response()->json(["code" => 200, "data" => $data, "jumlah" => count($data)]);
    } catch (JWTException $exception) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, user cannot be logged out'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}

public function fetchAduanHistoriAdmin(Request $request)
{
    try {
        $searchUser = $request->user;
        $user = JWTAuth::authenticate($request->token);
        $aduans = Aduan::join('users as u', 'u.id', '=', 'aduan.user_pengajuan_id')->select('aduan.id','aduan.latitude', 
                'aduan.longitude', 'aduan.lokasi', 'aduan.link_file', 'aduan.tipe_file', 'aduan.status', 'aduan.created_at', 
                'aduan.updated_at', 'u.name', 'response_time', 'departure_date', 'arrive_to_destination', 'finish_services')
            ->where('u.name', 'like', '%'.$searchUser.'%')
            ->orderBy('aduan.created_at', 'DESC')
            ->get();
        $data = [];
        foreach ($aduans as $row) {
            if($row->status == '0'){
                $status = "Menunggu Verifikasi";
            }elseif ($row->status == '1') {
                $status = "Terverifikasi";
            }else{
                $status = "Tidak valid";
            }

            array_push($data, [
                'id' => $row->id,
                'name' => $row->name,
                'latitude' => $row->latitude,
                'longitude' => $row->longitude,
                'lokasi' => $row->lokasi,
                'link_file' => $row->link_file,
                'url_asset_image' => config('global.URL_ASSET_BARU').'fileaduans/'.$row->link_file,
                'tipe_file' => $row->tipe_file,
                'alamat' => $row->lokasi,
                'status' => $status,
                'response_time' => $row->response_time != null ? tanggalForHuman($row->response_time) : "-",
                'departure_date' => $row->departure_date != null ? tanggalForHuman($row->departure_date) : "-",
                'arrive_to_destination' => $row->arrive_to_destination != null ? tanggalForHuman($row->arrive_to_destination) : "-",
                'finish_services' => $row->finish_services != null ? tanggalForHuman($row->finish_services) : "-",
                'created_at' => tanggalForHuman($row->created_at),
                'updated_at' => tanggalForHuman($row->updated_at)
            ]);
        }
        return response()->json(["code" => 200, "data" => $data]);
    }catch (JWTException $exception) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, user cannot be logged out'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}

public function konfirmasiTindakLanjut(Request $request)
{
    try {
        $user = JWTAuth::authenticate($request->token);
        $id = $request->pengadu_user_id;
        $status = $request->status;
        $aduan = Aduan::find($id);
        $aduan->status = $request->status;
        $aduan->user_verifikator_id = $user->id;
        $aduan->response_time = Carbon::now();
        $aduan->save();
        if($request->status == "1"){
            $status = 'valid';
        }else if($request->status == "2"){
            $status = 'not-valid';
        }
        $this->sendNotification("Pengaduan", $aduan->user_pengajuan_id, $user->id, $status);
        return response()->json(["code" => 200, "message" => "berhasil melakukan konfirmasi"]);
    }catch (JWTException $exception) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, user cannot be logged out'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }  
}

public function sendKonfirmasi(Request $request)
{
    try {
        $user = JWTAuth::authenticate($request->token);
        $id = $request->pengadu_user_id;
        $status = $request->status;
        $aduan = Aduan::find($id);
        if($status == 'otw'){
            $aduan->departure_date = Carbon::now();
        // }elseif($status == 'arrived'){
        //     $aduan->arrive_to_destination = Carbon::now();
        }elseif($status == 'finish'){
            $aduan->finish_services = Carbon::now();
        }
        $aduan->user_verifikator_id = $user->id;
        $aduan->save();

        $this->sendNotification("Pengaduan", $aduan->user_pengajuan_id, $user->id, $status);

        return response()->json(["code" => 200, "message" => "berhasil melakukan konfirmasi"]);

      
    }catch (JWTException $exception) {
        return response()->json([
            'success' => false,
            'message' => 'Sorry, user cannot be logged out'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }  
}

public function sendNotification($title, $userId, $sender, $status)
{
    $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
    $token = "AAAAKi29oJI:APA91bGG5psJXoSKNItXtEeunf8XQo6aGB74fiq4TLTa1G5_VuAiB-nkThcaNQ1ZbFS1Rfm6bDpJ2cNptbBbLfrwtVnbXoxO29Esm8hzxKC2p9_24tAeAfdvf0OVUUsgq11VVl1g0i-5";

    $user = User::find($userId);
    $userSender = User::find($sender);

    $message = "";
    if($status == "not-valid"){
        $message = $userSender->name." telah menolak pengaduan anda";
    }elseif($status == "valid") {
        $message = $userSender->name." telah menerima pengaduan anda";
    }elseif($status == "otw"){
        $message = "Pengaduan anda telah diterima \ndan sedang dalam proses perjalanan";
    }elseif($status == "arrived"){
        $message = "Pengaduan anda telah diterima \ndan telah sampai di tujuan";
    }elseif($status == "finish"){
        $message = "Pengaduan anda telah diterima \ndan telah berhasil diselesaikan";
    }elseif ($status == "aduan_baru") {
        $message = $userSender->name." mengirim aduan baru, mohon tindak lanjut";
    }

    Notification::create([
        "user_id" => $userId,
        "content" => $message,
        "title" => $title,
        "is_readed" => 0
    ]);

    $notification = [
        'tipeNotifikasi' => "aduan",
        'title' => $title,
        'userId' => $userId,
        'namaUser' => $user->name,
        'senderUser' => $userSender->name,
        'status' => $status,
        'message' => $message
    ];

    // $notificationData = ["message" => $notification,"moredata" =>'dd'];

    $fcmNotification = [
        //'registration_ids' => $tokenList, 
        'to' => "/topics/ChattingApp",
        'data' => $notification
    ];

    $headers = [
        'Content-Type: application/json',
        'Authorization: key='.$token,
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    $response = curl_exec($ch);
    curl_close($ch);
    return response()->json($response);
}

}
