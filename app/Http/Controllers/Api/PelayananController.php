<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pelayanan;
use App\Models\Jadwal;
use DB;

class PelayananController extends Controller
{

    public function fetchTarifRanap()
    {
        $curlTarifRanap = curl_init();
        curl_setopt_array($curlTarifRanap, array(
            CURLOPT_URL => "http://192.168.100.88/promedika/api-referensi/info-tarif-kamar",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $responseTarif = curl_exec($curlTarifRanap);
        curl_close($curlTarifRanap);
        $tarifRanap = json_decode($responseTarif, true);
        return $tarifRanap['data'];
    }
    
    public function fetchIndex()
    {
        $pelayanan = Pelayanan::where('is_active', 1)->get();
        return response()->json(["code" => 200, "data" => $pelayanan]);
    }

    public function detailIndex($id, $tipe)
    {
        $dataItem = [];
        if($tipe == 'not-direct') {
            $pelayanan = DB::table('detail_pelayanan as dp')->where('dp.pelayanan_id', $id)
                ->select('dp.nama_det', 'dp.id', 'dp.pelayanan_id')
                ->get();
        }
        else if($tipe == 'not-direct-tarif'){
            $pelayanan = [];
            $photos = [];
            $layananParent = DB::table('detail_pelayanan as dp')->where('dp.pelayanan_id', $id)
                ->select('dp.nama_det', 'dp.id', 'dp.deskripsi_pel')
                ->get();
            foreach ($layananParent as $row) {
                $layananSecond = DB::table('foto_det_pel as foto')
                    ->where('foto.id_det_pel', $row->id)
                    ->select('foto.foto_det_pel as foto')
                    ->get();
                foreach ($layananSecond as $s) {
                    array_push($photos,[
                      "photo" => $s->foto
                    ]);
                }
                array_push($pelayanan, [
                    'id' => $row->id,
                    'nama_det' => $row->nama_det,
                    'deskripsi_pel' => strval($row->deskripsi_pel),
                    'photos' => $photos
                ]);
                $photos = [];
            }
        }else if($tipe == 'direct'){
            $pelayanan = [];
            $photos = [];
            $layananParent = Pelayanan::where('pelayanan.is_active', 1)
                ->where('pelayanan.id', $id)
                ->get();
            foreach ($layananParent as $row) {
                $layananSecond = DB::table('detail_pelayanan as dp')->where('dp.pelayanan_id', $row->id)
                    ->get();
                foreach ($layananSecond as $s) {
                    array_push($photos,[
                        "photo" => $s->foto
                    ]);
                }
                array_push($pelayanan, [
                    'id' => $row->id,
                    'nama_det' => $row->nama,
                    'deskripsi_pel' => strval($row->deskripsi),
                    'photos' => $photos
                ]);
                $photos = [];
            }
        }else if($tipe == 'empty'){
            $pelayanan = [];
        }

        if($tipe == 'not-direct-tarif'){
            $tarifRanap = $this->fetchTarifRanap();
            foreach ($tarifRanap as $row) {
                array_push($dataItem, [
                    "item1" => $row["product_name"],
                    "item2" => rupiah($row["price"])
                ]);
            }
        }else{
            $jadwal = Jadwal::where('pelayanan_id', $id)
                    ->select('hari_1', 'hari_2', 'jam_1', 'jam_2', 'is_24_jam')
                    ->get();
           
            foreach ($jadwal as $i) {
                if($i->is_24_jam == 0){
                    if($i->hari_2 != null){
                        $item1 = strval($i->hari_1.' s.d '.$i->hari_2);
                        $item2 = strval($i->jam_1.' - '.$i->jam_2);
                    }else{
                        $item1 = strval($i->hari_1);
                        $item2 = strval($i->jam_1.' - '.$i->jam_2);
                    }
                }else{
                    $item1 = "Senin s.d Minggu";
                    $item2 = "24 Jam";
                }
                array_push($dataItem, [
                    'item1' => $item1,
                    'item2' => $item2
                ]);
            }
        }
        
        return response()->json(["code" => 200, "data" => $pelayanan, "item" => $dataItem]);
    }

    public function detailIndex2($id)
    {
        $pelayanan = [];
        $photos = [];
        $layananParent = DB::table('detail_pelayanan as dp')->where('dp.id', $id)
            ->select('dp.nama_det', 'dp.id', 'dp.deskripsi_pel')
            ->get();
        foreach ($layananParent as $row) {
            $layananSecond = DB::table('foto_det_pel as foto')
                ->where('foto.id_det_pel', $row->id)
                ->select('foto.foto_det_pel as foto')
                ->get();
            foreach ($layananSecond as $s) {
                array_push($photos,[
                  "photo" => $s->foto
                ]);
            }
            array_push($pelayanan, [
                'id' => $row->id,
                'nama_det' => $row->nama_det,
                'deskripsi_pel' => strval($row->deskripsi_pel),
                'photos' => $photos
            ]);
            $photos = [];
        }

        $dataItem = [];

        $idPelayanan = 0;
        if($id == 14){
            $idPelayanan = 14;
        }elseif ($id == 15) {
            $idPelayanan = 15;
        }else{
            $idPelayanan = 7;
        }
        $jadwal = Jadwal::where('pelayanan_id', $idPelayanan)
            ->select('hari_1', 'hari_2', 'jam_1', 'jam_2', 'is_24_jam')
            ->get();

        foreach ($jadwal as $i) {
            if($i->is_24_jam == 0){
                if($i->hari_2 != null){
                    $item1 = strval($i->hari_1.' s.d '.$i->hari_2);
                    $item2 = strval($i->jam_1.' - '.$i->jam_2);
                }else{
                    $item1 = strval($i->hari_1);
                    $item2 = strval($i->jam_1.' - '.$i->jam_2);
                }
            }else{
                $item1 = "Senin s.d Minggu";
                $item2 = "24 Jam";
            }
            array_push($dataItem, [
                'item1' => $item1,
                'item2' => $item2
            ]);
        }
        return response()->json(["code" => 200, "data" => $pelayanan, "item" => $dataItem]);     
    }

}
