<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Popup;

class PopupController extends Controller
{
    
    public function fetchPopup()
    {
        $popup = Popup::where('is_active', 1)
            ->select('id', \DB::raw('CONCAT("'.config('global.URL_ASSET_BARU').'popup/'.'", konten) as konten'), 'url', 'button_action',
                    'title');
        return response()->json(["code" => 200, "data" => $popup->first(), "jumlah" => $popup->count()]);
    }

}
