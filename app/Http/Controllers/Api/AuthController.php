<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use Illuminate\Http\Request;
use App\Models\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{

     public function register(Request $request)
    {
        $userCount = User::where('email', $request->email)->count();
        if($userCount > 0){
            return response()->json([
                'success' => false,
                'message' => 'Username '.$request->email.' sudah digunakan, Silahkan gunakan yang lain!'
            ], Response::HTTP_OK);
        }else{

            $userCount2 = User::where('nik', $request->nik)->count();
            if($userCount2 > 0){
                return response()->json([
                    'success' => false,
                    'message' => 'NIK '.$request->nik.' sudah digunakan, Silahkan gunakan yang lain!'
                ], Response::HTTP_OK);
            }else{
                $userCount3 = User::where('telepon', $request->telepon)->count();
                if($userCount3 > 0){
                    return response()->json([
                        'success' => false,
                        'message' => 'No Whatsapp '.$request->telepon.' sudah digunakan, Silahkan gunakan yang lain!'
                    ], Response::HTTP_OK);
                }else{
                    $folderPath = "ktp/";

                    $url = config('global.URL_SERVER_ASSET')."upload-foto-ktp";

                    $data = array(
                        "file" => $request->foto_ktp, 
                    );
                    
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec($curl);
                    $result = json_decode($response, true);
                    $fileName = $result['message'];
    
                    $otp = random_int(100000, 999999);
                    
                    $user = User::create([
                        'name' => $request->name,
                        'email' => $request->email,
                        'telepon' => $request->telepon,
                        'nik' => $request->nik,
                        'foto_ktp' => $fileName,
                        'password' => bcrypt($request->password),
                        'user_type' => 'mobile',
                        'ip_address' => $request->ip_address,
                        'is_verified_otp' => 0,
                        'otp' => $otp
                    ]);
    
                    $this->sendOTP($request->telepon, $otp);
    
                    $credentials = $request->only('email', 'password');
    
                    try {
                        if (! $token = JWTAuth::attempt($credentials)) {
                            return response()->json([
                                'success' => false,
                                'message' => 'Login credentials are invalid.',
                            ], 400);
                        }
                    } catch (JWTException $e) {
                    return $credentials;
                        return response()->json([
                                'success' => false,
                                'message' => 'Could not create token.',
                            ], 500);
                    }
            
                    $user = User::where('email', $request->email)->first();
                
                    return response()->json([
                        'success' => true,
                        'token' => $token,
                        'userId' => $user->id,
                        'isVerifiedOTP' => $user->is_verified_otp == 0 ? "belum_otp" : "sudah_otp"
                    ]);
                }
            }
    
            // return response()->json([
            //     'success' => true,
            //     'message' => 'User created successfully',
            //     'data' => $user
            // ], Response::HTTP_OK);
        }
    }

    public function verifikasiOTP(Request $request)
    {
        $cek = User::where('id', $request->id)
            ->where('otp', $request->otp);
        if($cek->count() > 0){
            $cek->update([
                'is_verified_otp' => 1
            ]);
            $status = true;
            $statusOTp = 'sudah_otp';
            $message = 'Berhasil melakukan verifikasi otp';
        }else{
            $statusOTp = 'belum_otp';
            $status = false;
            $message = 'Gagal melakukan verifikasi otp, periksa kembali kode anda';
        }
        return response()->json(["code" => 200, 'success' => $status, 
            'message' => $message, 'isVerifiedOTP' => $statusOTp]);
    }

    public function kirimUlangOTP(Request $request)
    {
        $otp = random_int(100000, 999999);
        $sendOTP = User::where('id', $request->id);
        $user = $sendOTP->first();
        $sendOTP->update([
                'otp' => $otp
            ]);
        $this->sendOTP($user->telepon, $otp);
        return response()->json([
            'success' => true,
            'userId' => $request->id,
            'isVerifiedOTP' => "belum_otp"
        ]);
    }

    public function sendOTP($phone, $otp)
    {
        $message = "Kode VERIFIKASI anda adalah ".$otp.". Jangan infokan kode VERIFIKASI ke siapapun.";
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, config('global.URL_API_WA'));
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => config('global.USER_KEY_WA'),
            'passkey' => config('global.PASS_KEY_WA'),
            'to' => $phone,
            'message' => $message
        ));
        $results = json_decode(curl_exec($curlHandle), true);
        curl_close($curlHandle);
    }
 
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($credentials, [
            'email' => 'required|string',
            'password' => 'required|string|min:6|max:50'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                ], 500);
        }

        $user = User::where('email', $request->email)->first();
 	
        return response()->json([
            'success' => true,
            'token' => $token,
            'userId' => $user->id,
            'isVerifiedOTP' => $user->is_verified_otp
        ]);
    }

    public function getUserData($token)
    {
        $user = JWTAuth::authenticate($token);
        return $user;
    }
 
    public function logout(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->only('token'), [
            'token' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

		//Request is validated, do logout        
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                'success' => true,
                'message' => 'User has been logged out'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
 
    public function get_user(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        $user = JWTAuth::authenticate($request->token);
 
        return response()->json(['user' => $user]);
    }

    public function detailUser(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);
        return response()->json(['data' => $user, 'url_ktp' => config('global.URL_ASSET').'ktp/'.$user->foto_ktp]);
    }

    public function findNumberWA(Request $request)
    {
        $dtUser = User::where('telepon', $request->telepon)
            ->where('is_active', 1)
            ->first();
        $status = true;
        $message = "";
        if($dtUser != null){
            $otp = random_int(100000, 999999);
            $update = User::find($dtUser->id);
            $update->request_reset_password = 1;
            $update->kode_reset_password = $otp;
            $update->save();
            $this->sendOTP($dtUser->telepon, $otp);
            $message = "Kode verifikasi berhasil dikirim, silahkan cek nomor whatsapp";
        }else{
            $status = false;
            $message = "Nomor whatsapp tidak ditemukan, periksa kembali nomor yang dimasukkan";
        }
        return response()->json(["code" => 200, "status" => $status, "message" => $message]);
    }

    public function resetPassword(Request $request)
    {
        $dtUser = User::where('kode_reset_password', $request->kode_reset_password)
            ->where('request_reset_password', 1)
            ->first();
        $status = true;
        $message = "";
        if($dtUser != null){
            $user = User::find($dtUser->id);
            $user->password = bcrypt($request->password);
            $user->request_reset_password = 0;
            $user->kode_reset_password = null;
            $user->save();
            $message = "Berhasil melakukan reset password, silahkan login dengan password baru";
        }else{
            $status = false;
            $message = "Kode yang dimasukkan tidak valid";
        }
        return response()->json(["code" => 200, "status" => $status, "message" => $message]);
    }

    public function updateUser(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $dtUser = User::find($user->id);
            $dtUser->nik = $request->nik;
            $dtUser->name = $request->name;
            $dtUser->telepon = $request->telepon;
            if($request->foto_ktp != ""){
                $folderPath = "ktp/";
                $image_base64 = base64_decode($request->foto_ktp);
                $fileName = uniqid().'.png';
                $file = $folderPath . $fileName;
                file_put_contents($file, $image_base64);
                $dtUser->foto_ktp = $fileName;
            }
            if($request->password != ""){
                $dtUser->password = bcrypt($request->password);
            }
            $dtUser->save();
        }catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
 
        return response()->json([
            'success' => true,
            'message' => 'User updated successfully',
            'data' => $user
        ], Response::HTTP_OK);
    }

}