<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArtikelController extends Controller
{
    
    public function fechArtikel(Request $request)
    {
        $search = $request->search;
        $page = $request->per_page;
        $curl_artikel = curl_init();
        curl_setopt_array($curl_artikel, array(
            CURLOPT_URL => "https://rsudgambiran.kedirikota.go.id/wp-json/wp/v2/posts?_embed&per_page=".$page,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response_artikel = curl_exec($curl_artikel);
        curl_close($curl_artikel);
        $artikel = json_decode($response_artikel, true);
        $data = [];
        foreach($artikel as $row){
            if($search != ""){
                if(strpos($row['title']['rendered'], $search) != false){
                    array_push($data, [
                        "id" => $row['id'],
                        "date" => tanggalForHuman($row['date']),
                        "title" => $row['title']['rendered'],
                        "thumbnail_preview" => $row['_embedded']["wp:featuredmedia"][0]["source_url"],
                        "thumbnail" => $row['_embedded']["wp:featuredmedia"][0]["link"],
                        "content" => $row["content"]['rendered']
                    ]);
                }
            }else{
                array_push($data, [
                    "id" => $row['id'],
                    "date" => tanggalForHuman($row['date']),
                    "title" => $row['title']['rendered'],
                    "thumbnail_preview" => $row['_embedded']["wp:featuredmedia"][0]["source_url"],
                    "thumbnail" => $row['_embedded']["wp:featuredmedia"][0]["link"],
                    "content" => $row["content"]['rendered']
                ]);
            }
        }
        return response()->json(["code" => 200, "data" => $data]);
    }


}
