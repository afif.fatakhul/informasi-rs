<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Antrian;
use App\Models\BookingOnline;
use JWTAuth;

class SisbroController extends Controller
{

    public function fetchTime(Request $request)
    {
        $data = [];
        $initialTime = '07:32:00';
        $looping = 75;
        $day = Carbon::parse($request->tgl_antrian)->format('l');
        if($day == 'Friday'){
            $looping = 35;
        }
        for($i = 1; $i <= $looping; $i++){
           $time = Carbon::parse($initialTime)->addMinutes(3)->format('H:i');
           $initialTime = $time;
           $countBooking = Antrian::where('antrian_no', $i)
            ->where('status', 1)->whereDate('antrian_tgl', $request->tgl_antrian)->count();
           array_push($data, [
               "no_urut" => $i,
               "jam" => $time,
               "is_booked" => strval($countBooking)
           ]);
        }
        return response()->json(["data" => $data]);
    }

    public function getPatient($id)
    {
        // api-patient
        $curl_patient = curl_init();
        curl_setopt_array($curl_patient, array(
            CURLOPT_URL => config('global.SIMRS')."info-pasien/" . $id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response_patient = curl_exec($curl_patient);
        curl_close($curl_patient);
        $patient = json_decode($response_patient, true);

        return $patient["data"];
    }

    public function monitoringAntrian()
    {
        $curl_sisbro_online = curl_init();
        curl_setopt_array($curl_sisbro_online, array(
            CURLOPT_URL => config('global.SIMRS')."/antrean-loket",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response_sisbro_online = curl_exec($curl_sisbro_online);
        curl_close($curl_sisbro_online);
        $antrean = json_decode($response_sisbro_online, true);

        return response()->json(["code" => 200, "data" => $antrean]);
    }

    public function daftar(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            if($user->is_active == 1){
                $day = Carbon::parse($request->antrian_tgl)->format('l');
                if($day == 'Sunday'){
                    return response()->json(["success" => false, 
                        "jam" => "Peringatan",
                        "message" => "Antrian tidak dapat dibooking, dikarenakan hari libur"]
                    );
                }else{
                    $jamTanggal = $request->antrian_tgl . ' ' . $request->jam;
                    $cekData = Antrian::where('antrian_no', $request->no_urut)
                        ->where('antrian_tgl', $jamTanggal)
                        ->count();
                    $cekSudahBooking2Kali = Antrian::where('pasien_norm', $request->pasien_norm)
                        ->whereDate('antrian_tgl', $request->antrian_tgl)
                        ->count();
                    $cekSudahDaftarTglIniByPoli = Antrian::where('pasien_norm', $request->pasien_norm)
                        ->whereDate('antrian_tgl', $request->antrian_tgl)
                        ->first();
                   
                    if($cekSudahDaftarTglIniByPoli != null){
                        if($cekSudahBooking2Kali < 2){
                            if($cekSudahDaftarTglIniByPoli->pasien_carabayar == "BPJS PBI" || 
                                $cekSudahDaftarTglIniByPoli->pasien_carabayar == "BPJS NPBI" || 
                                $cekSudahDaftarTglIniByPoli->pasien_carabayar == "KOTA" || 
                                $cekSudahDaftarTglIniByPoli->pasien_carabayar == "KETENAGAKERJAAN" ||
                                $cekSudahDaftarTglIniByPoli->pasien_carabayar == "JAMKESDA KOTA" ||
                                $cekSudahDaftarTglIniByPoli->pasien_carabayar == "JAMKESDA PROPINSI"){
                                    if($request->pasien_carabayar != "UMUM"){
                                        return response()->json(["success" => false, 
                                            "title" => $request->pasien_norm,
                                            "message" => "Pada hari ".$request->antrian_tgl.", pasien ".$request->pasien_norm." sudah didaftarkan dengan cara bayar ".$cekSudahDaftarTglIniByPoli->pasien_carabayar." silahkan pilih dengan cara bayar UMUM"]
                                        );
                                    }else{
                                        $kodeAntrian = Carbon::parse($request->antrian_tgl)->format('Ymd') . \Str::random(7);
                                        $tglNowCreated = Carbon::now();
                                        $id = Antrian::insertGetId([
                                            'antrian_no' => $request->no_urut,
                                            'antrian_kode' => $kodeAntrian,
                                            'antrian_tgl' => $jamTanggal,
                                            'pasien_norm' => $request->pasien_norm,
                                            'pasien_nama' => $request->pasien_nama,
                                            'pasien_tgllahir' => $this->getPatient($request->pasien_norm)['date_of_birth'],
                                            'pasien_jk' => $this->getPatient($request->pasien_norm)['gender'],
                                            'pasien_alamat' => $this->getPatient($request->pasien_norm)['domicily_address'],
                                            'pasien_carabayar' => $request->pasien_carabayar,
                                            'pasien_tujuan' => $request->pasien_tujuan,
                                            'status' => '1',
                                            'antrian_status' => 'BELUM CHECKIN',
                                            'mobile_user_id' => $user->id,
                                            'created_at' => $tglNowCreated
                                        ]);
                                        return response()->json(["data" => $id, "success" => true, 
                                            "kode_antrian" => $kodeAntrian,
                                            "antrian_kode" => $kodeAntrian,
                                            "antrian_tgl" => $request->antrian_tgl,
                                            "status" => '1',
                                            "title" => $request->jam,
                                            "antrian_created_at" => formatTanggalJamSistem($tglNowCreated),
                                            "message" => "Berhasil melakukan booking antrian"
                                        ]);
                                    }
                                }
                        }else{
                            return response()->json(["success" => false, 
                                "title" => $request->pasien_norm,
                                "message" => "Pada hari ".$request->antrian_tgl.", pasien ".$request->pasien_norm." sudah didaftarkan 2 kali"]
                            );
                        }
                    }
                    else if($cekData > 0){
                        return response()->json(["success" => false, 
                            "title" => "Jam ".$request->jam,
                            "message" => "Antrian tersebut sudah dibooking, Silahkan pilih nomor lainnya"]
                        );
                    }else{
                        $hariBooking = Carbon::parse($request->antrian_tgl)->format('l');
                        if($request->pasien_tujuan == "POLI BEDAH SYARAF*"){
                            if($hariBooking == 'Monday' || $hariBooking == 'Wednesday' || $hariBooking == 'Friday'){
                                $kodeAntrian = Carbon::parse($request->antrian_tgl)->format('Ymd') . \Str::random(7);
                                $tglNowCreated = Carbon::now();
                                $id = Antrian::insertGetId([
                                    'antrian_no' => $request->no_urut,
                                    'antrian_kode' => $kodeAntrian,
                                    'antrian_tgl' => $jamTanggal,
                                    'pasien_norm' => $request->pasien_norm,
                                    'pasien_nama' => $request->pasien_nama,
                                    'pasien_tgllahir' => $this->getPatient($request->pasien_norm)['date_of_birth'],
                                    'pasien_jk' => $this->getPatient($request->pasien_norm)['gender'],
                                    'pasien_alamat' => $this->getPatient($request->pasien_norm)['domicily_address'],
                                    'pasien_carabayar' => $request->pasien_carabayar,
                                    'pasien_tujuan' => $request->pasien_tujuan,
                                    'status' => '1',
                                    'antrian_status' => 'BELUM CHECKIN',
                                    'mobile_user_id' => $user->id,
                                    'created_at' => $tglNowCreated
                                ]);
                                return response()->json(["data" => $id, "success" => true, 
                                    "kode_antrian" => $kodeAntrian,
                                    "antrian_kode" => $kodeAntrian,
                                    "antrian_tgl" => $request->antrian_tgl,
                                    "status" => '1',
                                    "title" => $request->jam,
                                    "antrian_created_at" => formatTanggalJamSistem($tglNowCreated),
                                    "message" => "Berhasil melakukan booking antrian"
                                ]);
                            }else{
                                return response()->json(["success" => false, 
                                    "title" => "Gagal Booking",
                                    "message" => "Poli Bedah Syaraf hanya buka pada hari Senin, Rabu, dan Jumat"]
                                );
                            }
                        }else if($request->pasien_tujuan == "POLI BEDAH ANAK"){
                            if($hariBooking == 'Tuesday' || $hariBooking == 'Thursday'){
                                $kodeAntrian = Carbon::parse($request->antrian_tgl)->format('Ymd') . \Str::random(7);
                                $tglNowCreated = Carbon::now();
                                $id = Antrian::insertGetId([
                                    'antrian_no' => $request->no_urut,
                                    'antrian_kode' => $kodeAntrian,
                                    'antrian_tgl' => $jamTanggal,
                                    'pasien_norm' => $request->pasien_norm,
                                    'pasien_nama' => $request->pasien_nama,
                                    'pasien_tgllahir' => $this->getPatient($request->pasien_norm)['date_of_birth'],
                                    'pasien_jk' => $this->getPatient($request->pasien_norm)['gender'],
                                    'pasien_alamat' => $this->getPatient($request->pasien_norm)['domicily_address'],
                                    'pasien_carabayar' => $request->pasien_carabayar,
                                    'pasien_tujuan' => $request->pasien_tujuan,
                                    'status' => '1',
                                    'antrian_status' => 'BELUM CHECKIN',
                                    'mobile_user_id' => $user->id,
                                    'created_at' => $tglNowCreated
                                ]);
                                return response()->json(["data" => $id, "success" => true, 
                                    "kode_antrian" => $kodeAntrian,
                                    "antrian_kode" => $kodeAntrian,
                                    "antrian_tgl" => $request->antrian_tgl,
                                    "status" => '1',
                                    "title" => $request->jam,
                                    "antrian_created_at" => formatTanggalJamSistem($tglNowCreated),
                                    "message" => "Berhasil melakukan booking antrian"
                                ]);
                            }else{
                                return response()->json(["success" => false, 
                                    "title" => "Gagal Booking",
                                    "message" => "Poli Bedah Anak hanya buka pada hari Selasa dan Kamis"]
                                );
                            }
                        }else{
                            $kodeAntrian = Carbon::parse($request->antrian_tgl)->format('Ymd') . \Str::random(7);
                            $tglNowCreated = Carbon::now();
                            $id = Antrian::insertGetId([
                                'antrian_no' => $request->no_urut,
                                'antrian_kode' => $kodeAntrian,
                                'antrian_tgl' => $jamTanggal,
                                'pasien_norm' => $request->pasien_norm,
                                'pasien_nama' => $request->pasien_nama,
                                'pasien_tgllahir' => $this->getPatient($request->pasien_norm)['date_of_birth'],
                                'pasien_jk' => $this->getPatient($request->pasien_norm)['gender'],
                                'pasien_alamat' => $this->getPatient($request->pasien_norm)['domicily_address'],
                                'pasien_carabayar' => $request->pasien_carabayar,
                                'pasien_tujuan' => $request->pasien_tujuan,
                                'status' => '1',
                                'antrian_status' => 'BELUM CHECKIN',
                                'mobile_user_id' => $user->id,
                                'created_at' => $tglNowCreated
                            ]);
                            return response()->json(["data" => $id, "success" => true, 
                                "kode_antrian" => $kodeAntrian,
                                "antrian_kode" => $kodeAntrian,
                                "antrian_tgl" => $request->antrian_tgl,
                                "status" => '1',
                                "title" => $request->jam,
                                "antrian_created_at" => formatTanggalJamSistem($tglNowCreated),
                                "message" => "Berhasil melakukan booking antrian"
                            ]);
                        }
                    }
                }
            }else{
                return response()->json(["code" => 200, "message" => "User anda tidak aktif", "success" => false]);
            }
        }catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function caraBayar()
    {
        $curl_cara_bayar = curl_init();
        curl_setopt_array($curl_cara_bayar, array(
            CURLOPT_URL => config('global.SIMRS')."info-cara-bayar",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response_cara_bayar = curl_exec($curl_cara_bayar);
        curl_close($curl_cara_bayar);

        $cara_bayar = json_decode($response_cara_bayar, true);

        $listCaraBayar = [];
        foreach ($cara_bayar['data'] as $row) {
            $text = $row['name'] == "BPJS PBI" ? "BPJS PBI / KIS" : $row['name'];
            array_push($listCaraBayar, [
                "value" => $row['name'],
                "text" => $text
            ]);
        }
        return response()->json(["code" => 200, "data" => $listCaraBayar]);
    }

    public function getPoli(Request $request)
    {
        $search = strtoupper($request->search);
        $curl_poli = curl_init();
        curl_setopt_array($curl_poli, array(
            CURLOPT_URL => config('global.SIMRS')."info-department/POLI",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response_poli = curl_exec($curl_poli);
        curl_close($curl_poli);

        $poli = json_decode($response_poli, true);

        $listPoli = [];
        foreach ($poli['data'] as $row) {
            if($search != ""){
                if(strpos($row["name"], $search) != false){
                    array_push($listPoli, [
                        "value" => $row['name'],
                        "text" => $row['name']
                    ]);
                }
            }else{
                array_push($listPoli, [
                    "value" => $row['name'],
                    "text" => $row['name']
                ]);
            }
        }
        return response()->json(["code" => 200, "data" => $listPoli]);
    }

    public function verifikasiKode(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $cekData = BookingOnline::where('user_id', $user->id)->where('referral_code', $request->referral_code);
            if($cekData->count() > 0){
                User::where('id', $user->id)->update([
                    'able_to_booking' => 1
                ]);
                $cekData->update([
                    'is_enable' => 1
                ]);
                $success = true;
                $message = 'Berhasil melakukan verifikasi kode';
            }else{
                $success = true;
                $message = 'Kode verifikasi tidak ditemukan';
            }
            return response()->json(["code" => 200, 'success' => $success, "message" => $message]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function fetchPasien($pasienMR)
    {
        $curl_patient = curl_init();
        curl_setopt_array($curl_patient, array(
            CURLOPT_URL => config('global.SIMRS')."info-pasien/" . $pasienMR,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));
        $response_patient = curl_exec($curl_patient);
        curl_close($curl_patient);
        $patient = json_decode($response_patient, true);
        $pasien = new \stdClass();
        $pasien->pasien_norm = $patient['data']['mr'];
        $pasien->pasien_nama = $patient['data']['first_name'];
        $pasien->pasien_tgllahir = $patient['data']['date_of_birth'];
        $pasien->pasien_jk = $patient['data']['gender'];
        $pasien->pasien_alamat = $patient['data']['domicily_address'];
        return $pasien;
    }

    public function fetchPasienBooking(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $bookingOnline = BookingOnline::join('users as u', 'u.id', '=', 'booking_online.user_id')
                ->where('booking_online.user_id', $user->id)
                ->where('is_enable', 1)
                ->get();
            return response()->json(["code" => 200, "data" => $bookingOnline]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function sendOTP($phone, $otp)
    {
        $message = "Kode VERIFIKASI anda adalah ".$otp.". Jangan infokan kode VERIFIKASI ke siapapun.";
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, config('global.URL_API_WA'));
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => config('global.USER_KEY_WA'),
            'passkey' => config('global.PASS_KEY_WA'),
            'to' => $phone,
            'message' => $message
        ));
        $results = json_decode(curl_exec($curlHandle), true);
        curl_close($curlHandle);
    }

    public function sendPasienFromSimrs(Request $request)
    {
        $user = User::find($request->user_id);
        $no_rm = $request->no_rm;
        $cekBookingOnline = BookingOnline::where('no_rm', $no_rm)->count();
        $status = true;
        $message = "";
        if($cekBookingOnline > 0){
            $status = false;
            $message = "No RM sudah didaftarkan dengan akun lain";
        }else{
            $status = true;
            $message = "Berhasil tambah data dan cek kode verifikasi di nomor whatsapp";
            $otp = random_int(100000, 999999);
            $this->sendOTP($user->telepon, $otp);
            BookingOnline::create([
                'user_id' => $request->user_id,
                'no_rm' => $no_rm,
                'referral_code' => $otp,
                'pasien_nama' => $request->pasien_nama,
                'is_enable' => 0
            ]);
        }
        return response()->json(["code" => 200, "status" => $status, "message" => $message]);
    }

    public function daftarBookingSaya(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $antrian = Antrian::where('mobile_user_id', $user->id)
                ->select('antrian_no as no_urut', 'pasien_norm', 'antrian_kode', 'antrian_status',
                    'pasien_nama', 'antrian_tgl', 'pasien_carabayar', 'pasien_tujuan', 'created_at', 'status')
                ->orderBy('created_at', 'desc')
                ->get();
            $data = [];
            foreach ($antrian as $row) {
                array_push($data, [
                    'antrian_kode' => $row->antrian_kode,
                    'no_urut' => $row->no_urut,
                    'pasien_norm' => $row->pasien_norm,
                    'pasien_nama' => $row->pasien_nama,
                    'pasien_carabayar' => $row->pasien_carabayar,
                    'jam' => jamForHuman($row->antrian_tgl),
                    'pasien_tujuan' => $row->pasien_tujuan,
                    'antrian_status' => $row->antrian_status,
                    'antrian_tgl' => hanyaTglHuman($row->antrian_tgl),
                    'antrian_created_at' => formatTanggalJamSistem($row->created_at),
                    'status' => strval($row->status)
                ]);
            }
            return response()->json(["code" => 200, "data" => $data, "jumlah" => count($data)]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function daftarBookingSayaV2(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $antrian = Antrian::where('mobile_user_id', $user->id)
                ->where('pasien_norm', $request->pasien_norm)
                ->select('antrian_no as no_urut', 'pasien_norm', 'antrian_kode', 'antrian_status',
                    'pasien_nama', 'antrian_tgl', 'pasien_carabayar', 'pasien_tujuan', 'created_at', 'status')
                ->orderBy('created_at', 'desc')
                ->get();
            $data = [];
            foreach ($antrian as $row) {
                array_push($data, [
                    'antrian_kode' => $row->antrian_kode,
                    'no_urut' => $row->no_urut,
                    'pasien_norm' => $row->pasien_norm,
                    'pasien_nama' => $row->pasien_nama,
                    'pasien_carabayar' => $row->pasien_carabayar,
                    'jam' => jamForHuman($row->antrian_tgl),
                    'pasien_tujuan' => $row->pasien_tujuan,
                    'antrian_status' => $row->antrian_status,
                    'antrian_tgl' => hanyaTglHuman($row->antrian_tgl),
                    'antrian_created_at' => formatTanggalJamSistem($row->created_at),
                    'status' => strval($row->status)
                ]);
            }
            return response()->json(["code" => 200, "data" => $data, "jumlah" => count($data)]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function batal(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $batal = Antrian::where('mobile_user_id', $user->id)
                    ->where('antrian_kode', $request->antrian_kode)
                    ->update([
                        'status' => 0
                    ]);
            $getPasien = Antrian::where('mobile_user_id', $user->id)
                ->where('antrian_kode', $request->antrian_kode)
                ->first();
            return response()->json(["success" => true, 
                "jam" => $request->pasien_norm,
                "message" => "Antrian pada hari ".$request->antrian_tgl.", Pasien ".$getPasien->pasien_norm." berhasil dibatalkan"]
            );
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function detailUserRM($userId)
    {
        $user = User::where('id', $userId)->select('name', 'email')->first();
        $bookingOnline = BookingOnline::where('user_id', $userId)
                            ->select('no_rm', 'pasien_nama', 'is_enable')
                            ->get();
        return response()->json(["code" => 200, "listBooking" => $bookingOnline, "user" => $user]);
    }

    public function getDataUser(Request $request)
    {
        $search = $request->search;
        $users = User::where('user_type', 'mobile')
            ->where('name', 'like', '%'.$search.'%')
            ->orWhere('user_type', 'mobile')
            ->where('email', 'like', '%'.$search.'%')
            ->select('id', 'nik', 'name', 'email as username', 'telepon', 'able_to_booking', 'is_verified_otp')
            ->get();
        return response()->json(["code" => 200, "data" => $users]);
    }

}
