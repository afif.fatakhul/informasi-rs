<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use JWTAuth;

class NotificationController extends Controller
{
    
    public function fetchData(Request $request)
    {
        try {
            $user = JWTAuth::authenticate($request->token);
            $notifications = Notification::where('user_id', $user->id)
                ->orderBy('created_at', 'DESC')
                ->get();
            $data = [];
            foreach ($notifications as $row) {
                array_push($data,[
                    'id' => $row->id,
                    'title' => $row->title,
                    'content' => $row->content,
                    'user_id' => $row->user_id,
                    'created_at' => tanggalForHuman($row->created_at)
                ]);
            }
            return response()->json(["code" => 200, "data" => $data, "jumlah" => count($data)]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
