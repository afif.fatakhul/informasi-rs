<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ruangan;
use App\Models\DetailRuangan;
use App\Models\Dokter;

class PoliController extends Controller
{
    
    public function fetchPoli(Request $request)
    {
        $search = $request->search;
        $limit = $request->limit;
        $jumlahData = Ruangan::where('kategori', 'POLI')->where('nama_ruangan', 'like', '%'.$search.'%')
            ->where('is_active', 1)->count();
        $poli = Ruangan::where('kategori', 'POLI')->where('nama_ruangan', 'like', '%'.$search.'%')
            ->where('is_active', 1)
            ->limit($limit)
            ->get();
        return response()->json(["code" => 200, "data" => $poli, "jumlahData" => $jumlahData]);
    }

    public function fetchGaleriPoli($id)
    {
        $detailRuangan = DetailRuangan::where('ruangan_id', $id)->get();
        return response()->json(["code" => 200, "data" => $detailRuangan]);
    }

    public function fetchGaleriPoliv2($id)
    {
        $detailRuangan = DetailRuangan::where('ruangan_id', $id)
            ->select('ruangan_id', \DB::raw('CONCAT("'.config('global.URL_ASSET_BARU').'poli/'.'", gambar_ruangan) as gambar_ruangan'))
            ->get();
        return response()->json(["code" => 200, "data" => $detailRuangan]);
    }

    public function listDokter($smf)
    {
        $dokterList = Dokter::where('smf', $smf)
            ->select('id', 'nama', \DB::raw('CONCAT("'.config('global.URL_ASSET_BARU').'dokter/'.'", gambar) as image_url'), 'smf as jenis', 'biografi', 'universitas')
            ->get();
        return response()->json(["code" => 200, "data" => $dokterList]);
    }

}
