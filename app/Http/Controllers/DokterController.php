<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dokter;
use App\Models\JenisDokter;
use App\Models\JadwalDokter;

class DokterController extends Controller
{
    
    public function index()
    {
        $dokter = Dokter::all();
        return view('admin.dokter.index', compact('dokter'));
    }

    public function create()
    {
        $jenis = Dokter::select('smf')->distinct()->get();
        return view('admin.dokter.create', compact('jenis'));
    }

    public function store(Request $request)
    {
        $resorce = $request->file('gambar');

        $url = config('global.URL_SERVER_ASSET')."upload-foto-dokter";
        $fileName = time().$_FILES['gambar']['name'];

        $fields = [
            'filedata' => new \CURLFile($_FILES['gambar']['tmp_name'], $_FILES['gambar']['type'], $fileName)
        ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_exec($ch);

        if(curl_exec($ch)){
            Dokter::create([
                "nama" => $request->nama,
                "smf" => $request->id_jns,
                "gambar" => $fileName,
                "universitas" => $request->universitas,
                "biografi" => $request->biografi
            ]);
            return redirect('dokter')->with("success", "Berhasil menambahkan dokter");
        }else{
            echo "Error!" . curl_error($ch);
        }

    }

    public function edit($id)
    {
        $dokter = Dokter::find($id);
        $jenis = Dokter::select('smf')->distinct()->get();
        return view('admin.dokter.edit', compact('dokter', 'jenis'));
    }

    public function update(Request $request, $id)
    {
        $dokter = Dokter::find($id);
        $resorce = $request->file('gambar');
        if($resorce != ""){
            $url = config('global.URL_SERVER_ASSET')."upload-foto-dokter";
            $fileName = time().$_FILES['gambar']['name'];
    
            $fields = [
                'filedata' => new \CURLFile($_FILES['gambar']['tmp_name'], $_FILES['gambar']['type'], $fileName)
            ];
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_exec($ch);
            $dokter->gambar = $fileName;
        }

        $dokter->nama = $request->nama;
        $dokter->smf = $request->smf;
        $dokter->universitas = $request->universitas;
        $dokter->biografi = $request->biografi;
        $dokter->save();
        return redirect('dokter')->with("success", "Berhasil mengubah dokter");
    }

    public function destroy($id)
    {
        $dokter = Dokter::find($id);
        $dokter->delete();
        return redirect('dokter')->with("success", "Berhasil menghapus dokter");
    }

    public function editJadwal($dokterId)
    {
        $jadwalSenin = JadwalDokter::where('dokter_id', $dokterId)
            ->where('hari', 'Senin')
            ->first();
        $jadwalSelasa = JadwalDokter::where('dokter_id', $dokterId)
            ->where('hari', 'Selasa')
            ->first();
        $jadwalRabu = JadwalDokter::where('dokter_id', $dokterId)
            ->where('hari', 'Rabu')
            ->first();
        $jadwalKamis = JadwalDokter::where('dokter_id', $dokterId)
            ->where('hari', 'Kamis')
            ->first();
        $jadwalJumat = JadwalDokter::where('dokter_id', $dokterId)
            ->where('hari', 'Jumat')
            ->first();
        $jadwalSabtu = JadwalDokter::where('dokter_id', $dokterId)
            ->where('hari', 'Sabtu')
            ->first();
        return view('admin.dokter.jadwal', compact(
            'jadwalSenin', 
            'jadwalSelasa', 
            'jadwalRabu', 
            'jadwalKamis', 
            'jadwalJumat', 
            'jadwalSabtu',
            'dokterId')
        );
    }

    public function inputJadwal(Request $request, $dokterId)
    {
        if($request->jadwalId != ""){
            $jadwalDokter = JadwalDokter::find($request->jadwalId);
        }else{
            $jadwalDokter = new JadwalDokter();
        }
        $jadwalDokter->hari = $request->hari;
        $jadwalDokter->jam_awal_prakter = $request->jam_awal_prakter;
        $jadwalDokter->jam_akhir_prakter = $request->jam_akhir_prakter;
        $jadwalDokter->dokter_id = $dokterId;
        $jadwalDokter->save();
        return redirect()->back()->with('success', 'Berhasil menambahkan jadwal dokter');
    }

}
