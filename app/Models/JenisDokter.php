<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisDokter extends Model
{
    use HasFactory;
    protected $connection = 'mysql2';
    protected $table = 'tb_jns3';
}
