<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailRuangan extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    protected $table = 'detail_ruangan';
    protected $fillable = ['gambar_ruangan', 'ruangan_id'];
}
