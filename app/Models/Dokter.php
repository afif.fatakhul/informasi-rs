<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    use HasFactory;
    //protected $connection = 'mysql2';
    protected $table = 'dpjp';
    protected $fillable = [
        "nama",
        "smf",
        "gambar",
        "ket",
        "universitas",
        "biografi"  
    ];
    public $timestamps = false;
}
