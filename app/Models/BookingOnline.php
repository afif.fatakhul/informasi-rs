<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingOnline extends Model
{
    use HasFactory;
    protected $table = 'booking_online';
    protected $fillable = [
        'user_id',
        'no_rm',
        'referral_code',
        'is_enable',
        'pasien_nama'
    ];
}
