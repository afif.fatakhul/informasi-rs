<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Aduan extends Model
{
    use HasFactory;
    protected $connection = 'mysql3';
    protected $table = 'aduan';
    protected $fillable = [
        "user_pengajuan_id",
        "latitude",
        "longitude",
        "lokasi",
        "link_file",
        "tipe_file",
        "status",  
        "keterangan",
        "sound_bell",
        "user_verifikator_id",
        "response_time",
        "departure_date",
        "arrive_to_destination",
        "finish_services"
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_pengajuan_id');
    }

}
