<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    protected $table = 'ruangan';
    protected $fillable = [
        "nama_ruangan",
        "code_ruangan",
        "kategori",
        "deskripsi",
        "fasilitas"    
    ];
}
