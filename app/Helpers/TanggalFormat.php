<?php

function formatTanggalJamSistem($tanggal) {
	return date("Y-m-d H:i:s", strtotime($tanggal));
}

function jamForHuman($tanggal){
	return date("H:i", strtotime($tanggal));
}

function hanyaTglHuman($tanggal){
	return date("d-m-Y", strtotime($tanggal));
}

function tanggalForHuman($tanggal){
	return date("d M Y H:i:s", strtotime($tanggal));
}