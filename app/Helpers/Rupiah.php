<?php

function rupiah($angka){
    $hasil_rupiah = number_format($angka,2,',','.');
    $hasil_rupiah = str_replace(",00","",$hasil_rupiah);
	return 'Rp '.$hasil_rupiah;
}